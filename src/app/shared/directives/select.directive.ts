import {AfterViewInit, Directive, ElementRef, HostListener, Input, OnDestroy, OnInit, Renderer2} from '@angular/core';
import { NgModel } from '@angular/forms';
import * as $ from 'jquery';

@Directive({
  selector: '[appSelect]'
})
export class SelectDirective implements OnInit, AfterViewInit, OnDestroy {
  el: ElementRef;
  constructor(el: ElementRef, private renderer: Renderer2) {
    this.el = el;
  }
  ngOnDestroy(): void {}
  ngOnInit(): void {}
  ngAfterViewInit(): any {
    const el = this.el.nativeElement;
    const $styledSelect = $(el);

    const $list = $styledSelect.next('ul');
    const $listItems = $list.children('li');

    $styledSelect.click(function(e) {
      e.stopPropagation();
      $('div.select-styled.active').not(this).each(function(){
        $(this).removeClass('active').next('ul.select-options').hide();
      });
      $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
      e.stopPropagation();
      $styledSelect.text($(this).text()).removeClass('active');
      $list.hide();
    });

    $(document).click(function() {
      $styledSelect.removeClass('active');
      $list.hide();
    });
  }
}
