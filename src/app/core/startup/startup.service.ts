import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ACLService } from '@delon/acl';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { ALAIN_I18N_TOKEN, MenuService, SettingsService, TitleService } from '@delon/theme';
import { TranslateService } from '@ngx-translate/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzIconService } from 'ng-zorro-antd/icon';
import { zip } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ICONS } from '../../../style-icons';
import { ICONS_AUTO } from '../../../style-icons-auto';
import { I18NService } from '../i18n/i18n.service';

/**
 * 用于应用启动时
 * 一般用来获取应用所需要的基础数据等
 */
@Injectable()
export class StartupService {
  constructor(
    iconSrv: NzIconService,
    private menuService: MenuService,
    private translate: TranslateService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    private settingService: SettingsService,
    private aclService: ACLService,
    private titleService: TitleService,
    private httpClient: HttpClient,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
  ) {
    iconSrv.addIcon(...ICONS_AUTO, ...ICONS);
  }

  load(): Promise<void> {
    // only works with promises
    // https://github.com/angular/angular/issues/15088
    return new Promise((resolve) => {
      zip(this.httpClient.get(`assets/tmp/i18n/${this.i18n.defaultLang}.json`), this.httpClient.get('assets/tmp/app-data.json'))
        .pipe(
          // Thông báo ngoại lệ được tạo sau khi nhận được các trình đánh chặn khác
          catchError((res) => {
            console.warn(`StartupService.load: Network request failed`, res);
            resolve();
            return [];
          }),
        )
        .subscribe(
          ([langData, appData]) => {
            // setting language data
            this.translate.setTranslation(this.i18n.defaultLang, langData);
            this.translate.setDefaultLang(this.i18n.defaultLang);

            // application data
            const res = appData as NzSafeAny;
            this.aclService.setFull(false);
            this.titleService.default = '';

            const tokenService = this.tokenService.get();

            if (
              typeof tokenService != 'undefined' &&
              tokenService != null &&
              typeof tokenService.token != 'undefined' &&
              tokenService.token != null
            ) {
              this.aclService.setRole(tokenService.authorities);
              this.menuService.resume();

              const app = {
                name: 'Nền tảng chia sẻ tích hợp',
                description: 'Ng-zorro admin panel front-end framework',
              };
              this.settingService.setApp(app);
              this.titleService.suffix = app.name;

              const user = {
                name: tokenService.userName,
                avatar: './assets/tmp/img/avatar.jpg',
                email: tokenService.email,
              };
              this.settingService.setUser(user);

              /*
              const rights = tokenService.rights.sort((n1: any, n2: any) => {
                if (n1.rightOrder > n2.rightOrder) {
                  return 1;
                }
                if (n1.rightOrder < n2.rightOrder) {
                  return -1;
                }
                return 0;
              });

              const parentsCustum: any = {
                text: '',
                i18n: 'menu.main',
                group: false,
                hideInBreadcrumb: true,
                children: [],
              };
              const childsCustum: any = [];

              // tách riêng menu parent và menu child
              const totalMenus = rights.length;
              for (let i = 0; i < totalMenus; i++) {
                const item = rights[i];
                item.rightCode = 'menu.' + rights[i].rightCode;
                if (item.parentId == 0) {
                  const object = {
                    id: item.id,
                    parentId: item.parentId,
                    text: '',
                    i18n: item.rightCode,
                    // icon: item.iconUrl,
                    link: item.urlRewrite,
                    icon: 'anticon-appstore',
                    shortcutRoot: true,
                    children: [],
                  };
                  parentsCustum.children.push(object);
                } else {
                  const object = {
                    id: item.id,
                    parentId: item.parentId,
                    text: '',
                    link: item.urlRewrite,
                    i18n: item.rightCode,
                    // badge: 2,
                  };
                  childsCustum.push(object);
                }
              }

              const totalChilds = childsCustum.length;
              const totalParents = parentsCustum.children.length;
              for (let i = 0; i < totalChilds; i++) {
                // vị trí child
                const child = childsCustum[i];
                for (let j = 0; j < totalParents; j++) {
                  // vị trí parent
                  const parent = parentsCustum.children[j];
                  if (child.parentId == parent.id) {
                    // add submenu parent
                    parentsCustum.children[j].children.push(child);
                  }
                }
              }

              const menu = [];
              menu.push(parentsCustum);
              console.log('menu', menu);
              this.menuService.add(menu);
              */
            }
          },
          () => {},
          () => {
            resolve();
          },
        );
    });
  }
}
