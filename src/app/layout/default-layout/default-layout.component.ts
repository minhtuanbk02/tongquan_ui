import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { INavData } from '@coreui/angular';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { AppConfig } from '../../app.config';
import { SsoService } from '../../service/sso.service';
import { navItems } from './_nav';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
})
export class DefaultLayoutComponent implements OnInit, OnDestroy, AfterViewInit {
  public sidebarMinimized = false;
  public navItems: INavData[] = [];

  // constructor(private ssoService: SsoService, @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService) {}

  toggleMinimize(e: any): any {
    this.sidebarMinimized = e;
  }
  ngOnInit(): void {
       this.navItems = navItems;
  }
  ngAfterViewInit(): void {}
  ngOnDestroy(): void {}
  logout(): void {
    // const token = this.tokenService.get();
    // if (token != null) {
    //   const url =
    //     AppConfig.settings.SSO_IS_URL +
    //     '/oidc/logout?post_logout_redirect_uri=' +
    //     AppConfig.settings.SSO_CALLBACK_URL +
    //     '&id_token_hint=' +
    //     token.accessTokenInfo.idToken;
    //   this.tokenService.clear();
    //   window.location.href = url;
    // }
  }
}
