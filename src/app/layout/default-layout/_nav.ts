import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Tông',
    url: '/agency',
    icon: 'icon-puzzle',
  },
  {
    name: 'Ứng dụng liên thông',
    url: '/application',
    icon: 'icon-puzzle',
  },
  {
    name: 'Đồng bộ',
    url: '/synchronized',
    icon: 'icon-calendar',
  },
  {
    name: 'Thống kê văn bản',
    url: '/report',
    icon: 'icon-graph',
    /*children: [
      {
        name: 'Văn bản đi',
        url: '/report/document-send',
        icon: 'icon-puzzle',
      },
      {
        name: 'Văn bản đến',
        url: '/report/document-arrive',
        icon: 'icon-puzzle',
      },
      {
        name: 'Văn bản trạng thái',
        url: '/report/document-status',
        icon: 'icon-puzzle',
      },
    ],*/
  },
];
