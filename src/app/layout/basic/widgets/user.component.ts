import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { StartupService } from '@core';
import { DA_SERVICE_TOKEN, ITokenService, SocialService } from '@delon/auth';
import { SettingsService, User } from '@delon/theme';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppConfig } from '../../../app.config';
import { AuthenticationService } from '../../../service/authentication.service';

@Component({
  selector: 'header-user',
  template: `
    <div class="alain-default__nav-item d-flex align-items-center px-sm" nz-dropdown nzPlacement="bottomRight" [nzDropdownMenu]="userMenu">
      <nz-avatar [nzSrc]="user.avatar" nzSize="small" class="mr-sm"></nz-avatar>
      {{ user.name }}
    </div>
    <nz-dropdown-menu #userMenu="nzDropdownMenu">
      <div nz-menu class="width-sm">
        <!--
        <div nz-menu-item routerLink="/pro/account/center">
          <i nz-icon nzType="user" class="mr-sm"></i>
          {{ 'menu.account.center' | translate }}
        </div>
        <div nz-menu-item routerLink="/pro/account/settings">
          <i nz-icon nzType="setting" class="mr-sm"></i>
          {{ 'menu.account.settings' | translate }}
        </div>
        -->
        <!--
        <div nz-menu-item routerLink="/exception/trigger">
          <i nz-icon nzType="close-circle" class="mr-sm"></i>
          {{ 'menu.account.trigger' | translate }}
        </div>
        -->
        <li nz-menu-divider></li>
        <div nz-menu-item (click)="logout()">
          <i nz-icon nzType="logout" class="mr-sm"></i>
          {{ 'menu.account.logout' | translate }}
        </div>
      </div>
    </nz-dropdown-menu>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [SocialService, AuthenticationService, SocialService, SettingsService, NzMessageService, StartupService, TranslateService],
})
export class HeaderUserComponent {
  get user(): User {
    return this.settings.user;
  }

  constructor(
    private authenticationService: AuthenticationService,
    private translateService: TranslateService,
    private settings: SettingsService,
    private router: Router,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
  ) {}

  logout(): void {
    // logout wso2 is
    const token = this.tokenService.get();
    if (token != null) {
      const url =
        AppConfig.settings.SSO_IS_URL + '/oidc/logout?post_logout_redirect_uri=' + AppConfig.settings.SSO_CALLBACK_URL + '&id_token_hint=' + token.accessTokenInfo.idToken;
      this.tokenService.clear();
      window.location.href = url;
    }

    // logout JWT
    // this.authenticationService
    //   .logout()
    //   .then((response) => {
    //     this.tokenService.clear();
    //     this.router.navigateByUrl(this.tokenService.login_url!);
    //   })
    //   .catch((response: any) => {
    //     this.tokenService.clear();
    //     this.router.navigateByUrl(this.tokenService.login_url!);
    //   });
  }
}
