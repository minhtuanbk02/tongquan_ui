import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { INavData } from '@coreui/angular';
import { _HttpClient } from '@delon/theme';
import { CookieService } from '@delon/util';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AdmRightService } from '../../service/adm-right.service';
import { AdmUserService } from '../../service/adm-user.service';

@Component({
  selector: 'app-layout-admin',
  templateUrl: './default-admin.component.html',
})
export class DefaultAdminComponent implements OnInit, OnDestroy, AfterViewInit {
  public sidebarMinimized = false;
  public navItems: INavData[] = [];

  constructor(
    private apimRightService: AdmRightService,
    private cookieService: CookieService,
    private el: ElementRef,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public msg: NzMessageService,
    private http: _HttpClient,
    private cdr: ChangeDetectorRef,
    private admUserService: AdmUserService,
    private router: Router,
    private modal: NzModalService,
    private translateService: TranslateService,
  ) {}

  toggleMinimize(e: any): any {
    this.sidebarMinimized = e;
  }
  ngOnInit(): void {
    const username = this.apimRightService.getITokenService();
    this.apimRightService.getRightListMenu(username?.userName, 'admin').then((response) => {
      response.data.forEach((item: any) => {
        item.children.forEach((xtem: any) => {
          if (xtem?.children?.length == 0) {
            delete xtem.children;
          }
        });
      });
      this.navItems = response.data;
      this.cdr.detectChanges();
    });
  }
  ngAfterViewInit(): void {}
  ngOnDestroy(): void {}
}
