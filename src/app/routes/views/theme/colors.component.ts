import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
// @ts-ignore
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';

@Component({
  templateUrl: 'colors.component.html'
})
export class ColorsComponent implements OnInit {
  constructor(@Inject(DOCUMENT) private _document: any) {}

  public themeColors(): void {
    // @ts-ignore
    Array.from(this._document.querySelectorAll('.theme-color')).forEach((el: HTMLElement) => {
      const background = getStyle('background-color', el);
      const table = this._document.createElement('table');
      table.innerHTML = `
        <table class="w-100">
          <tr>
            <td class="text-muted">HEX:</td>
            <td class="font-weight-bold">${rgbToHex(background)}</td>
          </tr>
          <tr>
            <td class="text-muted">RGB:</td>
            <td class="font-weight-bold">${background}</td>
          </tr>
        </table>
      `;
      // @ts-ignore
      el.parentNode.appendChild(table);
    });
  }

  ngOnInit(): void {
    this.themeColors();
  }
}
