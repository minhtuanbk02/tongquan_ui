import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { _HttpClient } from '@delon/theme';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Constants } from '../../../model/Constants';
import { ConstantsAuthor } from '../../../model/ConstantsAuthor';
import { AgencyService } from '../../../service/agency.service';

@Component({
  templateUrl: './agency.component.html',
})
export class AgencyComponent {
  private listCenter: any[] = [];
  PAGE_SIZE = Constants.PAGE_SIZE;
  nzTotal = 0;

  confirmModal?: NzModalRef;
  search = { name: '', code: '' };
  loading = false;
  private data: any[] = [];
  indeterminate = false;
  checked = false;
  setOfCheckedId = new Set<number>();

  constructor(
    public msg: NzMessageService,
    private http: _HttpClient,
    private cdr: ChangeDetectorRef,
    private agencyService: AgencyService,
    private router: Router,
    private modal: NzModalService,
    private translateService: TranslateService,
  ) {}

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit(): void {
    this.getData(0, this.PAGE_SIZE);
    // init data
    this.agencyService
      .getListCenter()
      .then((response) => {
        console.log('this.groups', response);
        this.listCenter = response.data;
      })
      .catch((response: any) => {
        console.log(response);
      });
  }

  onAllChecked(checked: boolean): void {
    this.data.filter(({ disabled }) => !disabled).forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.data.every((item) => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.data.some((item) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  changePageSize(size: any): void {
    this.PAGE_SIZE = size;
    this.getData(0, this.PAGE_SIZE);
  }

  getData(page: number, size: number): void {
    this.loading = true;
    console.log(this.search);
    this.agencyService
      .getPage(this.search, page, size)
      .then((response) => {
        this.loading = false;
        this.data = response.data.content;
        this.nzTotal = response.data.totalElements;
        this.cdr.detectChanges();

        this.setOfCheckedId.clear();
        this.refreshCheckedStatus();
      })
      .catch((response: any) => {
        this.loading = false;
        console.log(response);
      });
  }

  reset(): void {
    setTimeout(() => this.getData(0, this.PAGE_SIZE));
  }

  remove(data: any): void {
    this._remove(data);
  }

  _remove(data: any): void {
    this.translateService.get('message.change.delete').subscribe((resConfirm: string) => {
      this.confirmModal = this.modal.confirm({
        nzTitle: resConfirm,
        nzContent: '',
        nzOnOk: () => {
          if (data.centerCode === Constants.CODE_CENTER) {
            this.agencyService
              .delete({ agencyCode: data.code })
              .then((response) => {
                if (this.agencyService.isNotNull(response) && response.code == 200) {
                  this.msg.success(this.translateService.instant('message.success'));
                } else {
                  this.msg.error(this.translateService.instant('message.error'));
                }
                this.getData(0, this.PAGE_SIZE);
              })
              .catch((response: any) => {
                console.log(response);
              });
          } else {
            setTimeout(() => {
              this.translateService.get('message.error.centerCode').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }
        },
      });
    });
  }

  add(): void {
    this.router.navigateByUrl('/agency/add');
  }

  edit(data: any): void {
    if (data.centerCode != Constants.CODE_CENTER) {
      setTimeout(() => {
        this.translateService.get('message.error.centerCode').subscribe((res: string) => {
          this.msg.error(res);
        });
      }, 1000);
    } else {
      this.router.navigateByUrl('/agency/edit/' + data.id);
    }
  }
}
