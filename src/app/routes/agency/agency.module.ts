import { NgModule } from '@angular/core';

import { SharedModule } from '@shared';
import { AgencyRoutingModule } from './agency-routing.module';
import { AgencyBusinessComponent } from './business/agency-business.component';
import { AgencyComponent } from './list/agency.component';

@NgModule({
  imports: [AgencyRoutingModule, SharedModule],
  declarations: [AgencyComponent, AgencyBusinessComponent],
})
export class AgencyModule {}
