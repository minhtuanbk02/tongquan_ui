import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentSendComponent } from '../report/vanbandi/detail/document-send.component';
import { AgencyBusinessComponent } from './business/agency-business.component';
import { AgencyComponent } from './list/agency.component';

const routes: Routes = [
  {
    path: '',
    component: AgencyComponent,
    data: {
      title: 'Đơn vị liên thông',
    },
  },
  {
    path: 'add',
    component: AgencyBusinessComponent,
    data: {
      title: 'Thêm mới đơn vị liên thông',
    },
  },
  {
    path: 'edit/:id',
    component: AgencyBusinessComponent,
    data: {
      title: 'Chỉnh sửa đơn vị liên thông',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgencyRoutingModule {}
