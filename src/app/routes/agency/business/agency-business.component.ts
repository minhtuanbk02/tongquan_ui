import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Constants } from '../../../model/Constants';
import { AgencyService } from '../../../service/agency.service';

@Component({
  templateUrl: './agency-business.component.html',
})
export class AgencyBusinessComponent implements OnInit {
  form!: FormGroup;
  submitting = false;
  id: any;

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private agencyService: AgencyService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [null],
      pcode: [null, [Validators.required]],
      code: [null, [Validators.required]],
      name: [null, [Validators.required]],
      email: [null, []],
      mobile: [null, []],
      fax: [null, []],
    });

    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });

    if (this.agencyService.isNotNull(this.id)) {
      // edit
      this.agencyService
        .initEdit(this.id)
        .then((response) => {
          this.form.patchValue(response.data);
        })
        .catch((response: any) => {
          console.log(response);
        });
    }
  }
  submit(form: any): void {
    this.submitting = true;
    if (this.agencyService.isNull(this.id)) {
      if (form.pcode.trim() != Constants.CODE_CENTER) {
        setTimeout(() => {
          this.translateService.get('message.error.centerCode').subscribe((res: string) => {
            this.msg.error(res);
          });
        }, 1000);
        this.submitting = false;
      } else {
        this.agencyService
          .add(form)
          .then((response) => {
            if (this.agencyService.isNotNull(response) && response.code == 200) {
              setTimeout(() => {
                this.translateService.get('message.success.api').subscribe((res: string) => {
                  this.msg.success(res);
                });
              }, 1000);
              setTimeout(() => {
                this.back();
              }, 2000);
            } else {
              setTimeout(() => {
                this.translateService.get('message.error').subscribe((res: string) => {
                  this.msg.error(res);
                });
              }, 1000);
            }

            this.submitting = false;
            this.cdr.detectChanges();
          })
          .catch((response: any) => {
            console.log(response);
            this.submitting = false;
            this.cdr.detectChanges();
          });
      }
    } else {
      this.agencyService
        .add(form)
        .then((response) => {
          if (this.agencyService.isNotNull(response) && response.code == 200) {
            setTimeout(() => {
              this.translateService.get('message.success.api').subscribe((res: string) => {
                this.msg.success(res);
              });
            }, 1000);
            setTimeout(() => {
              this.back();
            }, 2000);
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
          this.cdr.detectChanges();
        });
    }
  }
  back(): void {
    this.router.navigateByUrl('/agency');
  }
}
