import { ChangeDetectorRef, Component, ElementRef, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { Constants } from '../../../../model/Constants';
import { AdmUserService } from '../../../../service/adm-user.service';

@Component({
  selector: 'app-adm-user-business',
  templateUrl: './adm-user-business.component.html',
})
export class AdmUserBusinessComponent implements OnInit {
  form = this.fb.group({
    id: [null],
    username: [null, [Validators.required]],
    status: [null, [Validators.required]],
    phone: [null, [Validators.pattern(/^[0-9]\d*$/)]],
    email: [null, [Validators.email]],
    address: [null, []],
    keys: [],
  });
  submitting = false;
  id: any;
  nzExpandAll = false;
  nzExpandAll_authority = false;
  keys?: any[];
  group: any[] = [];
  authorityByGroup: any[] = [];
  listStatus = Constants.STATUS_LIST_FULL;

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private admUserService: AdmUserService,
    private activatedRoute: ActivatedRoute,
    private el: ElementRef,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });

    if (this.admUserService.isNotNull(this.id)) {
      this.admUserService
        .initEdit(this.id)
        .then((response) => {
          this.group = response.data.treeGroup;
          this.authorityByGroup = response.data.treeAuthoritie;
          this.form.patchValue({
            id: response.data.id,
            username: response.data.username,
            status: response.data.status,
            phone: response.data.phone,
            email: response.data.email,
            address: response.data.address,
          });
          this.keys = response.data.keys;
        })
        .catch((response: any) => {
          console.log(response);
        });
    } else {
      this.admUserService.initAdd().then((response) => {
        this.group = response.treeGroup;
        this.authorityByGroup = response.treeAuthoritie;
      });
    }
  }

  submit(form: any): void {
    if (this.admUserService.validateForm(this.form, this.el)) {
      if (form.id) {
        form.keys = this.keys;
        this.submitting = true;
        this.admUserService
          .edit(form)
          .then((response) => {
            if (response.code == 1) {
              setTimeout(() => {
                this.msg.success(`Thành công`);
              }, 1000);
              this.back();
            } else {
              setTimeout(() => {
                this.msg.error(response.message);
              }, 1000);
            }

            this.submitting = false;
            this.cdr.detectChanges();
          })
          .catch((response: any) => {
            console.log(response);
            this.submitting = false;
            this.cdr.detectChanges();
          });
      } else {
        form.keys = this.keys;
        this.submitting = true;
        this.admUserService
          .add(form)
          .then((response) => {
            if (response.code == 1) {
              setTimeout(() => {
                this.msg.success(`Thành công`);
              }, 1000);
              this.back();
            } else {
              setTimeout(() => {
                this.msg.error(response.message);
              }, 1000);
            }

            this.submitting = false;
            this.cdr.detectChanges();
          })
          .catch((response: any) => {
            console.log(response);
            this.submitting = false;
            this.cdr.detectChanges();
          });
      }
    }
  }

  nzEvent(event: NzFormatEmitEvent): void {
    console.log('NzFormatEmitEvent', event, 'event.keys', event.keys, 'this.keys', this.keys);
    if (event.eventName == 'check') {
      this.keys = event.keys;
    }
    this.listAuthoritieByGroupId(event.node?.key);
  }

  listAuthoritieByGroupId(groupId: any): void {
    if (groupId != -1) {
      this.admUserService
        .listAuthoritieByGroupId(groupId)
        .then((response) => {
          this.authorityByGroup = response.treeAuthoritie;
          console.log(response);
        })
        .catch((response: any) => {
          console.log(response);
        });
    }
  }

  expand(): void {
    this.nzExpandAll = !this.nzExpandAll;
  }

  back(): void {
    this.router.navigateByUrl('/admin/system/adm-user');
  }
}
