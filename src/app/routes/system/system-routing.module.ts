import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ACLGuard } from '@delon/acl';
import { AdmAuthorityBusinessComponent } from './adm-authority/business/adm-authority-business.component';
import { AdmAuthorityComponent } from './adm-authority/list/adm-authority.component';
import { AdmDeptBusinessComponent } from './adm-dept/business/adm-dept-business.component';
import { AdmDeptComponent } from './adm-dept/list/adm-dept.component';
import { AdmGroupBusinessComponent } from './adm-group/business/adm-group-business.component';
import { AdmGroupComponent } from './adm-group/list/adm-group.component';
import { AdmParamBusinessComponent } from './adm-param/business/adm-param-business.component';
import { AdmParamComponent } from './adm-param/list/adm-param.component';
import { AdmRightBusinessComponent } from './adm-right/business/adm-right-business.component';
import { AdmRightComponent } from './adm-right/list/adm-right.component';
import { AdmUserBusinessComponent } from './adm-user/business/adm-user-business.component';
import { AdmUserComponent } from './adm-user/list/adm-user.component';

const routes: Routes = [
  {
    path: 'adm-dept',
    component: AdmDeptComponent,
    canActivate: [ACLGuard],
    data: { guard: 'ROLE_SYSTEM_DEPT_VIEW', title: '', titleI18n: 'app.title.dept' },
  },
  {
    path: 'adm-dept/add',
    component: AdmDeptBusinessComponent,
    canActivate: [ACLGuard],
    data: { guard: 'ROLE_SYSTEM_DEPT_ADD', title: '', titleI18n: 'app.title.dept.add' },
  },
  {
    path: 'adm-dept/edit/:id',
    component: AdmDeptBusinessComponent,
    canActivate: [ACLGuard],
    data: { guard: 'ROLE_SYSTEM_DEPT_EDIT', title: '', titleI18n: 'app.title.dept.edit' },
  },

  {
    path: 'adm-user',
    component: AdmUserComponent,
    canActivate: [ACLGuard],
    data: { guard: 'ROLE_SYSTEM_USER_VIEW', title: '', titleI18n: 'app.title.user' },
  },
  {
    path: 'adm-user/add',
    component: AdmUserBusinessComponent,
    canActivate: [ACLGuard],
    data: { guard: 'ROLE_SYSTEM_USER_ADD', title: '', titleI18n: 'app.title.user.add' },
  },
  {
    path: 'adm-user/edit/:id',
    component: AdmUserBusinessComponent,
    canActivate: [ACLGuard],
    data: { guard: 'ROLE_SYSTEM_USER_EDIT', title: '', titleI18n: 'app.title.user.edit' },
  },

  {
    path: 'adm-group',
    component: AdmGroupComponent,
    canActivate: [ACLGuard],
    data: { guard: 'ROLE_SYSTEM_GROUP_VIEW', title: '', titleI18n: 'app.title.group' },
  },
  {
    path: 'adm-group/add',
    component: AdmGroupBusinessComponent,
    canActivate: [ACLGuard],
    data: { guard: 'ROLE_SYSTEM_GROUP_ADD', title: '', titleI18n: 'app.title.group.add' },
  },
  {
    path: 'adm-group/edit/:id',
    component: AdmGroupBusinessComponent,
    canActivate: [ACLGuard],
    data: { guard: 'ROLE_SYSTEM_GROUP_EDIT', title: '', titleI18n: 'app.title.group.edit' },
  },

  {
    path: 'adm-authorities',
    component: AdmAuthorityComponent,
    data: { guard: 'ROLE_SYSTEM_AUTHORITY_VIEW', title: '', titleI18n: 'app.title.authorities' },
  },
  {
    path: 'adm-authorities/add',
    component: AdmAuthorityBusinessComponent,
    data: { guard: 'ROLE_SYSTEM_AUTHORITY_ADD', title: '', titleI18n: 'app.title.authorities.add' },
  },
  {
    path: 'adm-authorities/edit/:id',
    component: AdmAuthorityBusinessComponent,
    data: { guard: 'ROLE_SYSTEM_AUTHORITY_EDIT', title: '', titleI18n: 'app.title.authorities.add' },
  },

  {
    path: 'adm-right',
    component: AdmRightComponent,
    data: { guard: 'ROLE_SYSTEM_AUTHORITY_VIEW', title: '', titleI18n: 'app.title.authorities' },
  },
  {
    path: 'adm-right/add',
    component: AdmRightBusinessComponent,
    data: { guard: 'ROLE_SYSTEM_AUTHORITY_ADD', title: '', titleI18n: 'app.title.authorities.add' },
  },
  {
    path: 'adm-right/edit/:id',
    component: AdmRightBusinessComponent,
    data: { guard: 'ROLE_SYSTEM_AUTHORITY_EDIT', title: '', titleI18n: 'app.title.authorities.add' },
  },

  {
    path: 'adm-param',
    component: AdmParamComponent,
    data: { guard: 'ROLE_SYSTEM_PARAM_VIEW', title: '', titleI18n: 'app.title.param' },
  },
  {
    path: 'adm-param/add',
    component: AdmParamBusinessComponent,
    data: { guard: 'ROLE_SYSTEM_PARAM_ADD', title: '', titleI18n: 'app.title.param.add' },
  },
  {
    path: 'adm-param/edit/:id',
    component: AdmParamBusinessComponent,
    data: { guard: 'ROLE_SYSTEM_PARAM_EDIT', title: '', titleI18n: 'app.title.param.add' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemRoutingModule {}
