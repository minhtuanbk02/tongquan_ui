import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ACLService } from '@delon/acl';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { AdmGroupService } from '../../../../service/adm-group.service';
import { AdmRightService } from '../../../../service/adm-right.service';
import { AdmUserService } from '../../../../service/adm-user.service';

@Component({
  selector: 'app-adm-right-business',
  templateUrl: './adm-right-business.component.html',
})
export class AdmRightBusinessComponent implements OnInit {
  listAuthorities: any[] = [];
  listParent: any[] = [];
  form = this.fb.group({
    id: [null],
    parentId: [null, [Validators.required]],
    rightCode: [null, [Validators.required]],
    name: [null, [Validators.required]],
    rightOrder: [null, [Validators.required]],
    url: [null, []],
    icon: [null, []],
    description: [null, []],
    authoritieId: [null, []],
    badgeSql: null,
  });
  submitting = false;
  id: any;
  isParent = true;

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private admRightService: AdmRightService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });

    // init data
    if (this.admRightService.isNull(this.id)) {
      this.admRightService
        .initAdd()
        .then((response) => {
          this.listParent = response.listParent;
          this.listAuthorities = response.authorities;
          console.log(response);
        })
        .catch((response: any) => {
          console.log(response);
        });
    } else {
      this.admRightService
        .initEdit(this.id)
        .then((response) => {
          this.listParent = response.data.listParent;
          this.listAuthorities = response.data.right.authorities;
          this.form.patchValue({
            id: response.data.right.id,
            parentId: response.data.right.parentId,
            rightCode: response.data.right.rightCode,
            name: response.data.right.name,
            rightOrder: response.data.right.rightOrder,
            url: response.data.right.url,
            icon: response.data.right.icon,
            description: response.data.right.description,
            authoritieId: response.data.right.authoritieId,
            badgeSql: response.data.right.badgeSql,
          });
        })
        .catch((response: any) => {
          console.log(response);
        });
    }

    this.form.controls.url.disable();
  }

  changeIsParent(form: any): void {
    if (form.parentId == 0) {
      this.form.controls.url.setValue('');
      this.form.controls.url.disable();
      this.form.controls.url.setValidators([]);
      this.isParent = true;
    } else {
      this.form.controls.url.enable();
      this.form.controls.url.setValidators([Validators.required]);
      this.isParent = false;
    }
  }

  submit(form: any): void {
    this.submitting = true;

    if (this.admRightService.isNull(this.id)) {
      this.admRightService
        .add(form)
        .then((response) => {
          if (this.admRightService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
            }, 1000);
            this.back();
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
        });
    } else {
      this.admRightService
        .edit(form)
        .then((response) => {
          if (this.admRightService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
              this.back();
            }, 1000);
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
        });
    }
  }

  back(): void {
    this.router.navigateByUrl('/admin/system/adm-right');
  }
}
