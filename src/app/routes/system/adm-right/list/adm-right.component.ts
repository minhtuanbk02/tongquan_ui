import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { STChange, STColumn, STData } from '@delon/abc/st';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { _HttpClient } from '@delon/theme';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Constants } from '../../../../model/Constants';
import { ConstantsAuthor } from '../../../../model/ConstantsAuthor';
import { AdmRightService } from '../../../../service/adm-right.service';

@Component({
  selector: 'app-adm-right',
  templateUrl: './adm-right.component.html',
})
export class AdmRightComponent implements OnInit {
  role_add = ConstantsAuthor.Right.add;
  role_edit = ConstantsAuthor.Right.edit;
  role_delete = ConstantsAuthor.Right.delete;
  status = Constants.STATUS_LIST;
  status_active = Constants.active;
  status_lock = Constants.lock;
  PAGE_SIZE = Constants.PAGE_SIZE;
  nzTotal = 0;
  confirmModal?: NzModalRef;

  search = { name: '', status: '', description: '' };
  loading = false;
  private data: any[] = [];
  indeterminate = false;
  checked = false;
  setOfCheckedId = new Set<number>();

  constructor(
    public msg: NzMessageService,
    private http: _HttpClient,
    private cdr: ChangeDetectorRef,
    private admRightService: AdmRightService,
    private router: Router,
    private modal: NzModalService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.getData(0, this.PAGE_SIZE);
  }

  onAllChecked(checked: boolean): void {
    this.data.filter(({ disabled }) => !disabled).forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.data.every((item) => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.data.some((item) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  changePageSize(size: any): void {
    this.PAGE_SIZE = size;
    this.getData(0, this.PAGE_SIZE);
  }

  getData(page: number, size: number): void {
    page = page - 1;
    this.loading = true;
    this.admRightService
      .getPage(this.search, page, size)
      .then((response) => {
        this.loading = false;
        this.data = response.data.content;
        this.nzTotal = response.data.totalElements;
        this.cdr.detectChanges();

        this.setOfCheckedId.clear();
        this.refreshCheckedStatus();
      })
      .catch((response: any) => {
        this.loading = false;
        console.log(response);
      });
  }

  reset(): void {
    setTimeout(() => this.getData(0, this.PAGE_SIZE));
  }

  remove(item: any): void {
    const list: any[] = [];
    list.push(item.id);
    this._remove(list);
  }

  _remove(list: any): void {
    this.translateService.get('message.change.delete').subscribe((resConfirm: string) => {
      this.confirmModal = this.modal.confirm({
        nzTitle: resConfirm,
        nzContent: '',
        nzOnOk: () => {
          this.admRightService
            .delete(list)
            .then((response) => {
              if (this.admRightService.isNotNull(response) && response.code == 1) {
                setTimeout(() => {
                  this.translateService.get('message.success').subscribe((res: string) => {
                    this.msg.success(res);
                  });
                }, 1000);
              } else {
                setTimeout(() => {
                  this.translateService.get('message.error').subscribe((res: string) => {
                    this.msg.error(res);
                  });
                }, 1000);
              }

              this.getData(0, this.PAGE_SIZE);
            })
            .catch((response: any) => {
              console.log(response);
            });
        },
      });
    });
  }

  removeList(): void {
    const list: any[] = [];
    for (const id of this.setOfCheckedId) {
      list.push(id);
    }
    this._remove(list);
  }

  lock(item: any): void {
    const list: any[] = [];
    list.push(item.id);
    this._lock(list);
  }

  _lock(list: any): void {
    this.translateService.get('message.change.lock').subscribe((resConfirm: string) => {
      this.confirmModal = this.modal.confirm({
        nzTitle: resConfirm,
        nzContent: '',
        nzOnOk: () => {
          this.admRightService
            .lock(list)
            .then((response) => {
              if (this.admRightService.isNotNull(response) && response.code == 1) {
                setTimeout(() => {
                  this.translateService.get('message.success').subscribe((res: string) => {
                    this.msg.success(res);
                  });
                }, 1000);
              } else {
                setTimeout(() => {
                  this.translateService.get('message.error').subscribe((res: string) => {
                    this.msg.error(res);
                  });
                }, 1000);
              }

              this.getData(0, this.PAGE_SIZE);
            })
            .catch((response: any) => {
              console.log(response);
            });
        },
      });
    });
  }

  lockList(): void {
    const list: any[] = [];
    for (const id of this.setOfCheckedId) {
      list.push(id);
    }
    this._lock(list);
  }

  unlock(item: any): void {
    const list: any[] = [];
    list.push(item.id);
    this._unlock(list);
  }

  _unlock(list: any): void {
    this.translateService.get('message.change.unlock').subscribe((resConfirm: string) => {
      this.confirmModal = this.modal.confirm({
        nzTitle: resConfirm,
        nzContent: '',
        nzOnOk: () => {
          this.admRightService
            .unlock(list)
            .then((response) => {
              if (this.admRightService.isNotNull(response) && response.code == 1) {
                setTimeout(() => {
                  this.translateService.get('message.success').subscribe((res: string) => {
                    this.msg.success(res);
                  });
                }, 1000);
              } else {
                setTimeout(() => {
                  this.translateService.get('message.error').subscribe((res: string) => {
                    this.msg.error(res);
                  });
                }, 1000);
              }

              this.getData(0, this.PAGE_SIZE);
            })
            .catch((response: any) => {
              console.log(response);
            });
        },
      });
    });
  }

  unlockList(): void {
    const list: any[] = [];
    for (const id of this.setOfCheckedId) {
      list.push(id);
    }
    this._unlock(list);
  }

  add(): void {
    this.router.navigateByUrl('/admin/system/adm-right/add');
  }

  edit(item: any): void {
    this.router.navigateByUrl('/admin/system/adm-right/edit/' + item.id);
  }

  collapse(dataSelect: any, idx: any, $event: boolean): void {
    if ($event) {
      this.admRightService
        .loadChildrenByParentId(dataSelect.id)
        .then((response) => {
          console.log(response);
          this.data[idx].children = response;
        })
        .catch((response: any) => {
          console.log(response);
        });
    } else {
      this.data[idx].children = [];
    }
  }
}
