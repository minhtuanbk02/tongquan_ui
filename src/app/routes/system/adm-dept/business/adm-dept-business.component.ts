import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { AdmDeptService } from '../../../../service/adm-dept.service';

@Component({
  selector: 'app-adm-dept-business',
  templateUrl: './adm-dept-business.component.html',
})
export class AdmDeptBusinessComponent implements OnInit {
  form!: FormGroup;
  submitting = false;
  id: any;
  listParent: any[] = [];
  nzExpandAll = false;
  groups: any[] = [];
  keys?: any[];

  nzExpandDetail = false;
  groupDetails: any[] = [];

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private deptService: AdmDeptService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [null],
      deptName: [null, [Validators.required]],
      parentId: [0, [Validators.required]],
      deptCode: [null, [Validators.required]],
      deptDesc: [null, []],
      keys: [null],
    });

    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = params.get('id');
      // alert(this.id);
    });

    // init data
    this.deptService
      .initAdd()
      .then((response) => {
        console.log('this.groups', response);
        this.groups = response;
      })
      .catch((response: any) => {
        console.log(response);
      });
    this.deptService
      .loadListParent()
      .then((response) => {
        console.log('this.listParent', response);
        this.listParent = response;
      })
      .catch((response: any) => {
        console.log(response);
      });

    if (this.deptService.isNotNull(this.id)) {
      // edit
      this.deptService
        .initEdit(this.id)
        .then((response) => {
          this.form.patchValue({
            id: response.data.id,
            deptName: response.data.deptName,
            parentId: response.data.parentId,
            deptCode: response.data.deptCode,
            deptDesc: response.data.deptDesc,
          });
        })
        .catch((response: any) => {
          console.log(response);
        });
    }
  }

  submit(form: any): void {
    this.submitting = true;
    form.keys = this.keys;
    if (this.deptService.isNull(this.id)) {
      this.deptService
        .add(form)
        .then((response) => {
          if (this.deptService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
            }, 1000);
            this.back();
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
          this.cdr.detectChanges();
        });
    } else {
      this.deptService
        .edit(form)
        .then((response) => {
          if (this.deptService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
              this.back();
            }, 1000);
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
          this.cdr.detectChanges();
        });
    }
  }

  getAuthorityByGroup(event: any): void {
    if (event.node.key == '-1') {
    } else {
      this.deptService
        .getAuthorityByGroup(event.node.key)
        .then((response) => {
          console.log('this.groupDetails', response.treeData);
          this.groupDetails = response.treeData;
        })
        .catch((response: any) => {
          console.log(response);
        });
    }
  }

  nzEvent(event: NzFormatEmitEvent): void {
    if (event.eventName == 'check') {
      this.keys = event.keys;
      console.log('NzFormatEmitEvent', event, 'event.keys', event.keys, 'this.keys', this.keys);
    }
  }

  expand(): void {
    this.nzExpandAll = !this.nzExpandAll;
  }

  back(): void {
    this.router.navigateByUrl('/admin/system/adm-dept');
  }
}
