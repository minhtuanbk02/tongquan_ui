import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ACLService } from '@delon/acl';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { AdmGroupService } from '../../../../service/adm-group.service';
import { AdmUserService } from '../../../../service/adm-user.service';

@Component({
  selector: 'app-adm-group-business',
  templateUrl: './adm-group-business.component.html',
})
export class AdmGroupBusinessComponent implements OnInit {
  nzExpandAll = false;
  authority: any[] = [];
  form!: FormGroup;
  submitting = false;
  keys?: any[];
  id: any;

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private admUserService: AdmUserService,
    private admGroupService: AdmGroupService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private aclService: ACLService,
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [null],
      groupName: [null, [Validators.required]],
      description: [null, [Validators.required]],
      keys: [],
    });

    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = params.get('id');
      // alert(this.id);
    });

    // init data
    if (this.admGroupService.isNull(this.id)) {
      this.admGroupService
        .initAdd()
        .then((response) => {
          this.authority = response;
        })
        .catch((response: any) => {
          console.log(response);
        });
    } else {
      this.admGroupService
        .initEdit(this.id)
        .then((response) => {
          this.authority = response.treeData;
          this.form.patchValue({
            id: response.group.id,
            groupName: response.group.groupName,
            description: response.group.description,
            keys: response.keys,
          });
          this.keys = response.keys;
        })
        .catch((response: any) => {
          console.log(response);
        });
    }
  }

  submit(form: any): void {
    form.keys = this.keys;

    this.submitting = true;

    if (this.admGroupService.isNull(this.id)) {
      this.admGroupService
        .add(form)
        .then((response) => {
          if (this.admGroupService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
            }, 1000);
            this.back();
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
          this.cdr.detectChanges();
        });
    } else {
      this.admGroupService
        .edit(form)
        .then((response) => {
          if (this.admGroupService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
              this.back();
            }, 1000);
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
          this.cdr.detectChanges();
        });
    }
  }

  nzEvent(event: NzFormatEmitEvent): void {
    if (event.eventName == 'check') {
      this.keys = event.keys;
      console.log('NzFormatEmitEvent', event, 'event.keys', event.keys, 'this.keys', this.keys);
    }
  }

  back(): void {
    this.router.navigateByUrl('/admin/system/adm-group');
  }

  expand(): void {
    this.nzExpandAll = !this.nzExpandAll;
  }
}
