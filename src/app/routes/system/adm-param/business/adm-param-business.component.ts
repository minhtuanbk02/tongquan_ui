import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AdmParamService } from '../../../../service/adm-param.service';

@Component({
  selector: 'app-adm-param-business',
  templateUrl: './adm-param-business.component.html',
})
export class AdmParamBusinessComponent implements OnInit {
  listParent: any[] = [];
  form!: FormGroup;
  submitting = false;
  id: any;
  isParent = true;

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private admParamService: AdmParamService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [null],
      name: [null, [Validators.required]],
      value: [null, [Validators.required]],
      description: [null, []],
    });

    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = params.get('id');
      // alert(this.id);
    });

    // init data
    if (this.admParamService.isNotNull(this.id)) {
      this.admParamService
        .initEdit(this.id)
        .then((response) => {
          this.form.patchValue({
            id: response.data.id,
            name: response.data.name,
            value: response.data.value,
            description: response.data.description,
          });
        })
        .catch((response: any) => {
          console.log(response);
        });
    }
  }

  submit(form: any): void {
    this.submitting = true;

    if (this.admParamService.isNull(this.id)) {
      this.admParamService
        .add(form)
        .then((response) => {
          if (this.admParamService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
            }, 1000);
            this.back();
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
        });
    } else {
      this.admParamService
        .edit(form)
        .then((response) => {
          if (this.admParamService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
              this.back();
            }, 1000);
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
        });
    }
  }

  back(): void {
    this.router.navigateByUrl('/admin/system/adm-param');
  }
}
