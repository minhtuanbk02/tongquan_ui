import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AdmAuthorityService } from '../../../../service/adm-authority.service';

@Component({
  selector: 'app-adm-authority-business',
  templateUrl: './adm-authority-business.component.html',
})
export class AdmAuthorityBusinessComponent implements OnInit {
  form = this.fb.group({
    id: [null],
    authKey: [null, [Validators.required]],
    parentId: [0, [Validators.required]],
    authoritieName: [null, [Validators.required]],
    orderId: [null, [Validators.required]],
    description: [null, []],
  });
  submitting = false;
  id: any;
  listParent: any[] = [];

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authorityService: AdmAuthorityService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = params.get('id');
      // alert(this.id);
    });

    this.authorityService
      .loadListParent()
      .then((response) => {
        console.log('this.listParent', response);
        this.listParent = response;
      })
      .catch((response: any) => {
        console.log(response);
      });

    if (this.authorityService.isNotNull(this.id)) {
      this.authorityService
        .initEdit(this.id)
        .then((response) => {
          this.form.patchValue({
            id: response.data.id,
            authKey: response.data.authKey,
            parentId: response.data.parentId,
            authoritieName: response.data.authoritieName,
            orderId: response.data.orderId,
            description: response.data.description,
          });
        })
        .catch((response: any) => {
          console.log(response);
        });
    }
  }

  submit(form: any): void {
    this.submitting = true;
    if (this.authorityService.isNull(this.id)) {
      this.authorityService
        .add(form)
        .then((response) => {
          if (this.authorityService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
            }, 1000);
            this.back();
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
          this.cdr.detectChanges();
        });
    } else {
      this.authorityService
        .edit(form)
        .then((response) => {
          if (this.authorityService.isNotNull(response) && response.code == 1) {
            setTimeout(() => {
              this.translateService.get('message.success').subscribe((res: string) => {
                this.msg.success(res);
              });
              this.back();
            }, 1000);
          } else {
            setTimeout(() => {
              this.translateService.get('message.error').subscribe((res: string) => {
                this.msg.error(res);
              });
            }, 1000);
          }

          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
          this.cdr.detectChanges();
        });
    }
  }

  back(): void {
    this.router.navigateByUrl('/admin/system/adm-authorities');
  }
}
