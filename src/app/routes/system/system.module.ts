import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { AdmAuthorityBusinessComponent } from './adm-authority/business/adm-authority-business.component';
import { AdmAuthorityComponent } from './adm-authority/list/adm-authority.component';
import { AdmDeptBusinessComponent } from './adm-dept/business/adm-dept-business.component';
import { AdmDeptComponent } from './adm-dept/list/adm-dept.component';
import { AdmGroupBusinessComponent } from './adm-group/business/adm-group-business.component';
import { AdmGroupComponent } from './adm-group/list/adm-group.component';
import { AdmParamBusinessComponent } from './adm-param/business/adm-param-business.component';
import { AdmParamComponent } from './adm-param/list/adm-param.component';
import { AdmRightBusinessComponent } from './adm-right/business/adm-right-business.component';
import { AdmRightComponent } from './adm-right/list/adm-right.component';
import { AdmUserBusinessComponent } from './adm-user/business/adm-user-business.component';
import { AdmUserComponent } from './adm-user/list/adm-user.component';
import { SystemRoutingModule } from './system-routing.module';

const COMPONENTS = [
  AdmUserComponent,
  AdmUserBusinessComponent,
  AdmGroupComponent,
  AdmGroupBusinessComponent,
  AdmAuthorityComponent,
  AdmAuthorityBusinessComponent,
  AdmRightComponent,
  AdmRightBusinessComponent,
  AdmParamComponent,
  AdmParamBusinessComponent,
  AdmDeptComponent,
  AdmDeptBusinessComponent,
];

@NgModule({
  imports: [CommonModule, SharedModule, SystemRoutingModule],
  declarations: [...COMPONENTS],
})
export class SystemModule {}
