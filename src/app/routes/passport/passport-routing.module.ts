import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutPassportComponent } from '../../layout/passport/passport.component';
import { SsoProcessingComponent } from './sso-processing/sso-processing.component';
import { SsoComponent } from './sso/sso.component';

const routes: Routes = [
  // passport
  {
    path: '',
    component: LayoutPassportComponent,
    children: [
      {
        path: 'login',
        component: SsoComponent,
        data: { title: 'Đăng nhập', titleI18n: 'app.login.login' },
      },
      {
        path: 'processing',
        component: SsoProcessingComponent,
        data: { title: 'Đăng nhập', titleI18n: 'app.login.login' },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PassportRoutingModule {}
