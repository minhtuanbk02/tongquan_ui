import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppConfig } from '../../../app.config';
import { Constants } from '../../../model/Constants';
import { SsoService } from '../../../service/sso.service';

@Component({
  selector: 'app-sso',
  templateUrl: './sso.component.html',
})
export class SsoComponent implements OnInit {
  private returnUrl: any;

  constructor(private route: ActivatedRoute, private ssoService: SsoService) {}

  ngOnInit(): void {
    localStorage.setItem('on-boarding', '1');
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
    localStorage.setItem(Constants.RETURN_URL, this.returnUrl);
    console.log(AppConfig.settings.SSO_IS_URL);
    this.ssoService.initSso();
  }
}
