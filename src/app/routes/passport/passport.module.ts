import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { PassportRoutingModule } from './passport-routing.module';
import { SsoProcessingComponent } from './sso-processing/sso-processing.component';
import { SsoComponent } from './sso/sso.component';

const COMPONENTS = [SsoComponent, SsoProcessingComponent];

@NgModule({
  imports: [SharedModule, PassportRoutingModule],
  declarations: [...COMPONENTS],
})
export class PassportModule {}
