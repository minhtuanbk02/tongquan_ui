import { Component, Inject, OnDestroy, OnInit, Optional } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StartupService } from '@core';
import { ReuseTabService } from '@delon/abc/reuse-tab';
import { DA_SERVICE_TOKEN, ITokenService, SocialOpenType, SocialService } from '@delon/auth';
import { SettingsService, _HttpClient } from '@delon/theme';
import { environment } from '@env/environment';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Constants } from '../../../model/Constants';
import { TokenInfo } from '../../../model/token-info';
import { UserInfo } from '../../../model/user-info';
import { AuthenticationService } from '../../../service/authentication.service';
import { SsoService } from '../../../service/sso.service';

@Component({
  selector: 'app-sso-processing',
  templateUrl: './sso-processing.component.html',
  providers: [SocialService, SettingsService, NzMessageService, StartupService],
})
export class SsoProcessingComponent implements OnInit {
  private tokenInfo: any;
  private userInfo: any;
  private returnUrl: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private ssoService: SsoService,
    private settingsService: SettingsService,
    private socialService: SocialService,
    @Optional()
    @Inject(ReuseTabService)
    private reuseTabService: ReuseTabService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private startupSrv: StartupService,
    public http: _HttpClient,
    public msg: NzMessageService,
  ) {}

  ngOnInit(): void {
    localStorage.setItem('on-boarding', '1');
    const error_description = this.activatedRoute.snapshot.queryParams.error_description;
    const error = this.activatedRoute.snapshot.queryParams.error;

    if (error != null && error_description != null && error.length > 0 && error_description.length > 0) {
      this.ssoService.initSso();
    }

    this.returnUrl = localStorage.getItem(Constants.RETURN_URL);
    if (!this.returnUrl) {
      this.returnUrl = '/home';
    }
    const code = this.getAuthorizationCode();

    this.authenticationService
      .getUserInfo(code)
      .then((response) => {
        this.userInfo = response.data;

        const now = new Date();
        now.setSeconds(now.getSeconds() + response.data.accessTokenInfo.expiresIn);
        const expireTime = now.getTime();

        if (this.returnUrl == '/home?submit=continue') {
          this.returnUrl = '/home';
        }

        this.reuseTabService.clear();

        response.data.expired = expireTime;
        response.data.email = response.data.email;
        response.data.id = response.data.userName;
        response.data.name = response.data.userName;
        response.data.time = response.data.accessTokenInfo.expiresIn;
        response.data.token = 'Bearer ' + response.data.accessTokenInfo.accessToken;

        this.tokenService.set(response.data);

        this.startupSrv.load().then(() => {
          let url = this.tokenService.referrer!.url || '/';
          if (url.includes('/auth')) {
            url = '/';
          }
          this.router.navigateByUrl(url);
        });
      })
      .catch((response: any) => {
        this.tokenService.clear();
        this.router.navigateByUrl(this.tokenService.login_url!);
      });
  }

  getAuthorizationCode(): string {
    const code = this.activatedRoute.snapshot.queryParams.code;
    return code;
  }
}
