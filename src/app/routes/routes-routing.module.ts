import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DefaultLayoutNewComponent} from '../layout/default-layout-new/default-layout-new.component';


const routes: Routes = [
 /* { path: 'auth', loadChildren: () => import('./passport/passport.module').then((m) => m.PassportModule) },*/
  {
    path: '',
    component: DefaultLayoutNewComponent,
 /*   canActivate: [SimpleGuard],
    canActivateChild: [SimpleGuard],*/
    data: {
      title: 'Trang chủ',
    },
    children: [
      { path: '', redirectTo: 'gioithieu', pathMatch: 'full' },
      {
        path: 'quytrinh',
        loadChildren: () => import('./quytrinhnghiepvu/quytrinhnghiepvu.module').then((m) => m.QuytrinhnghiepvuModule),
      },
      {
        path: 'gioithieu',
        loadChildren: () => import('./gioithieu/gioithieu.module').then((m) => m.GioithieuModule),
      },
      {
        path: 'ungdung',
        loadChildren: () => import('./ungdung/ungdung.module').then((m) => m.UngdungModule),
      },

      { path: 'dichvu',
        loadChildren: () => import('./dichvu/dichvu.module').then(m => m.DichvuModule)
      },
    ],
  },
  { path: 'home', redirectTo: 'gioithieu' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
/*      useHash: environment.useHash,*/
      // NOTICE: If you use `reuse-tab` component and turn on keepingScroll you can set to `disabled`
      // Pls refer to https://ng-alain.com/components/reuse-tab
      scrollPositionRestoration: 'top',
    }),
  ],
  exports: [RouterModule],
})
export class RouteRoutingModule {}
