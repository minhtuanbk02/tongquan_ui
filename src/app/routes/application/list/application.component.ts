import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { _HttpClient } from '@delon/theme';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Constants } from '../../../model/Constants';
import { ApplicationService } from '../../../service/application.service';

@Component({
  templateUrl: './application.component.html',
})
export class ApplicationComponent {
  private listCenter: any[] = [];
  PAGE_SIZE = Constants.PAGE_SIZE;
  nzTotal = 0;

  confirmModal?: NzModalRef;
  search = { name: '', code: '' };
  loading = false;
  private data: any[] = [];
  indeterminate = false;
  checked = false;
  setOfCheckedId = new Set<number>();

  constructor(
    public msg: NzMessageService,
    private http: _HttpClient,
    private cdr: ChangeDetectorRef,
    private applicationService: ApplicationService,
    private router: Router,
    private modal: NzModalService,
    private translateService: TranslateService,
  ) {}

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit(): void {
    this.getData(0, this.PAGE_SIZE);
  }

  onAllChecked(checked: boolean): void {
    this.data.filter(({ disabled }) => !disabled).forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.data.every((item) => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.data.some((item) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  changePageSize(size: any): void {
    this.PAGE_SIZE = size;
    this.getData(0, this.PAGE_SIZE);
  }

  getData(page: number, size: number): void {
    this.loading = true;
    console.log(this.search);
    this.applicationService
      .getPage(this.search, page, size)
      .then((response) => {
        this.loading = false;
        this.data = response.data.content;
        this.nzTotal = response.data.totalElements;
        this.cdr.detectChanges();

        this.setOfCheckedId.clear();
        this.refreshCheckedStatus();
      })
      .catch((response: any) => {
        this.loading = false;
        console.log(response);
      });
  }

  reset(): void {
    setTimeout(() => this.getData(0, this.PAGE_SIZE));
  }

  remove(data: any): void {
    this._remove(data);
  }

  _remove(data: any): void {
    this.translateService.get('message.change.delete').subscribe((resConfirm: string) => {
      this.confirmModal = this.modal.confirm({
        nzTitle: resConfirm,
        nzContent: '',
        nzOnOk: () => {
          this.applicationService
            .delete(data)
            .then((response) => {
              if (this.applicationService.isNotNull(response) && response.code == 200) {
                this.msg.success(this.translateService.instant('message.success'));
              } else {
                this.msg.error(this.translateService.instant('message.error'));
              }
              this.getData(0, this.PAGE_SIZE);
            })
            .catch((response: any) => {
              this.msg.error(this.translateService.instant('message.error'));
              console.log(response);
            });
        },
      });
    });
  }
  add(): void {
    this.router.navigateByUrl('/application/add');
  }

  edit(data: any): void {
    this.router.navigateByUrl('/application/edit/' + data.id);
  }
}
