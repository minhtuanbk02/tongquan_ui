import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Constants } from '../../../model/Constants';
import { AgencyService } from '../../../service/agency.service';
import { ApplicationService } from '../../../service/application.service';

@Component({
  templateUrl: './application-business.component.html',
})
export class ApplicationBusinessComponent implements OnInit {
  form!: FormGroup;
  submitting = false;
  id: any;
  private listAgecyVP: any[] = [];
  formAgency!: FormGroup;
  titleModal: any;
  isVisible = false;
  private listAgency: any[] = [];
  nzTotal = 0;
  private isAgencyExits = false;
  private isAppCodeExits = false;

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private agencyService: AgencyService,
    private applicationService: ApplicationService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [null],
      code: [null, [Validators.required, Validators.pattern('([0-9a-zA-Z])+')]],
      name: [null, [Validators.required]],
      manage: [null, [Validators.required]],
      email: [null, [Validators.email]],
      mobile: [null, [Validators.pattern('([0-9])+')]],
      fax: [null, []],
      listAgency: [null, []],
    });

    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });

    if (this.applicationService.isNotNull(this.id)) {
      // edit
      this.applicationService
        .initEdit(this.id)
        .then((response) => {
          this.form.patchValue(response.data);
          this.listAgency = response.data.listAgency;
        })
        .catch((response: any) => {
          console.log(response);
        });
    }
  }

  submit(form: any): void {
    this.submitting = true;
    form.listAgency = this.listAgency;
    if (this.applicationService.isNull(this.id)) {
      this.isAppCodeExits = false;
      this.applicationService
        .exitAppName(form.code)
        .then((response) => {
          if (this.applicationService.isNotNull(response) && response.code == 1) {
            this.isAppCodeExits = true;
          }
          if (this.isAppCodeExits) {
            this.msg.error(this.translateService.instant('message.error.appCode'));
            this.submitting = false;
          } else {
            this.applicationService
              .add(form)
              .then((data) => {
                if (this.applicationService.isNotNull(data) && data.code == 200) {
                  this.msg.success(this.translateService.instant('message.success'));
                  setTimeout(() => {
                    this.back();
                  }, 2000);
                } else {
                  this.msg.error(this.translateService.instant('message.error'));
                }
                this.submitting = false;
                this.cdr.detectChanges();
              })
              .catch((data: any) => {
                this.msg.error(this.translateService.instant('message.error'));
                this.submitting = false;
                this.cdr.detectChanges();
              });
          }
        })
        .catch((response: any) => {
          this.msg.error(this.translateService.instant('message.error'));
          this.submitting = false;
          this.cdr.detectChanges();
        });
    } else {
      this.applicationService
        .add(form)
        .then((response) => {
          if (this.applicationService.isNotNull(response) && response.code == 200) {
            this.msg.success(this.translateService.instant('message.success'));
            setTimeout(() => {
              this.back();
            }, 2000);
          } else {
            this.msg.error(this.translateService.instant('message.error'));
          }
          this.submitting = false;
          this.cdr.detectChanges();
        })
        .catch((response: any) => {
          console.log(response);
          this.submitting = false;
          this.cdr.detectChanges();
        });
    }
  }

  back(): void {
    this.router.navigateByUrl('/application');
  }

  handleCancel(): void {
    this.isVisible = false;
    this.formAgency.reset();
  }

  submitAgency(formAgency: any): void {
    this.checkExits(formAgency);
    if (this.isAgencyExits == false) {
      this.isVisible = false;
      this.listAgency = [...this.listAgency, formAgency];
      this.nzTotal = this.listAgency.length;
      this.cdr.detectChanges();
      this.formAgency.reset();
    } else {
      this.msg.error(this.translateService.instant('message.error.exit'));
      this.formAgency.reset();
      this.isVisible = false;
    }
  }

  checkExits(formAgency: any): boolean {
    this.isAgencyExits = false;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.listAgency.length; i++) {
      if (this.listAgency[i].name == formAgency.name) {
        this.isAgencyExits = true;
      }
    }
    return this.isAgencyExits;
  }

  listAgencVP(): void {
    this.formAgency = this.fb.group({
      id: [null],
      name: [null, [Validators.required]],
      code: [null, []],
    });
    this.agencyService
      .listAgencyVP()
      .then((response) => {
        if (this.agencyService.isNotNull(response) && response.code == 1) {
          this.listAgecyVP = response.data;
        }
        this.submitting = false;
      })
      .catch((response: any) => {
        this.submitting = false;
      });
  }

  //
  // fetchName(code: any): string {
  //   return this.listAgecyVP.filter((x) => x.code === code)[0].name;
  // }
  remove(idx: any): void {
    const lst = [];
    for (let i = 0; i < this.listAgency.length; i++) {
      if (idx != i) {
        lst.push(this.listAgency[i]);
      }
    }
    // this.listAgency = this.listAgency.splice(idx, 1);
    this.listAgency = lst;
    this.nzTotal = this.listAgency.length;
  }
}
