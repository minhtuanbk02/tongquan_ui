import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentSendComponent } from '../report/vanbandi/detail/document-send.component';
import { ApplicationBusinessComponent } from './business/application-business.component';
import { ApplicationComponent } from './list/application.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationComponent,
    data: {
      title: 'Ứng dụng liên thông',
    },
  },
  {
    path: 'add',
    component: ApplicationBusinessComponent,
    data: {
      title: 'Thêm mới ứng dụng',
    },
  },
  {
    path: 'edit/:id',
    component: ApplicationBusinessComponent,
    data: {
      title: 'Chỉnh sửa ứng dụng',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationRoutingModule {}
