import { NgModule } from '@angular/core';

import { SharedModule } from '@shared';
import { ApplicationRoutingModule } from './application-routing.module';
import { ApplicationBusinessComponent } from './business/application-business.component';
import { ApplicationComponent } from './list/application.component';

@NgModule({
  imports: [ApplicationRoutingModule, SharedModule],
  declarations: [ApplicationComponent, ApplicationBusinessComponent],
})
export class ApplicationModule {}
