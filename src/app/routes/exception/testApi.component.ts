import {HttpClient, HttpClientModule, HttpHeaders} from '@angular/common/http';
import {Component, Injectable, OnInit} from '@angular/core';

@Component({
  selector: 'exception-500',
  template: ` <exception type="500" style="min-height: 500px; height: 80%;"></exception> `,
})

@Injectable()
export class TestApiComponent implements OnInit{
  constructor(private http: HttpClient){}

  ngOnInit(): void {
    const url = 'https://lgsp-api.vinhphuc.gov.vn/lgsp-dmdc/1.0.0/provinces/all';
    const token = 'eyJ4NXQiOiJNell4TW1Ga09HWXdNV0kwWldObU5EY3hOR1l3WW1NNFpUQTNNV0kyTkRBelpHUXpOR00wWkdSbE5qSmtPREZrWkRSaU9URmtNV0ZoTXpVMlpHVmxOZyIsImtpZCI6Ik16WXhNbUZrT0dZd01XSTBaV05tTkRjeE5HWXdZbU00WlRBM01XSTJOREF6WkdRek5HTTBaR1JsTmpKa09ERmtaRFJpT1RGa01XRmhNelUyWkdWbE5nX1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJhZG1pbiIsImF1dCI6IkFQUExJQ0FUSU9OIiwiYXVkIjoiQzF2TDh1bk1pbGlXMW1YR2dzM2JmRmI3S3dRYSIsIm5iZiI6MTYzOTQ4MTkyNiwiYXpwIjoiQzF2TDh1bk1pbGlXMW1YR2dzM2JmRmI3S3dRYSIsInNjb3BlIjoiZGVmYXVsdCIsImlzcyI6Imh0dHBzOlwvXC9hcGltLWxnc3AudmluaHBodWMuZ292LnZuOjQ0M1wvb2F1dGgyXC90b2tlbiIsImV4cCI6OTIyMzM3MjAzNjg1NDc3NSwiaWF0IjoxNjM5NDgxOTI2LCJqdGkiOiIxZmZkZDAwNy0yN2IxLTQ3MzYtYWQ5NS1iNDAzZjJhOTk0NDIifQ.uMgZ5QSlj44-ec1GdqevsuIYFT1lmHKLv98ubxiFx7ZYtjdnXroTFnrnaVvFFD3AsO31QAr1quX4WjnIZAksUmTZmNYMQy8Pr8_9m4LiL37Up2OoX9Uvd-_q8Vu8SJ3taPSnHatc4ufWQxSwfyGLCONPK7fE9wt3z3GcsBRk3ES8ykW4_W_B35qifPfW8rFui90SFZCnUiGPfNMl1OPdAjvWu0mbP9M5Y_4CcHKmF4C8JgY3wSRml_v3IYaARZmdrOPMRSeBjZu5rlYTRzHr-9qOi4Qa1MR3-6O6_D-G4lWVCB8BEhY1hp1ppoQf4jAueceXWAqRp_SyPKK7Ms4w8A';
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append('Authorization', `Bearer ${token}`);
    const  result = this.http.get(url, {headers: secureHeaders}).toPromise()
      .then((response) => response as any)
      .catch((error) => {
        console.log(error);
      });
  }
}
