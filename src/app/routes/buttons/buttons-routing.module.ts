import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BrandButtonsComponent } from './brand-buttons.component';
import { ButtonsComponent } from './buttons.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Buttons',
    },
    children: [
      {
        path: '',
        redirectTo: 'buttons',
      },
      {
        path: 'buttons',
        component: ButtonsComponent,
        data: {
          title: 'Buttons',
        },
      },
      {
        path: 'brand-buttons',
        component: BrandButtonsComponent,
        data: {
          title: 'Brand buttons',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ButtonsRoutingModule {}
