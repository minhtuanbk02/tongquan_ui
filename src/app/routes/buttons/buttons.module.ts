import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { BrandButtonsComponent } from './brand-buttons.component';
import { ButtonsComponent } from './buttons.component';

// Dropdowns Component
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Buttons Routing
import { ButtonsRoutingModule } from './buttons-routing.module';

// Angular

@NgModule({
  imports: [CommonModule, ButtonsRoutingModule, BsDropdownModule.forRoot(), FormsModule],
  declarations: [ButtonsComponent, BrandButtonsComponent],
})
export class ButtonsModule {}
