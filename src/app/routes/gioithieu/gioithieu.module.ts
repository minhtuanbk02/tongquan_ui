import { NgModule } from '@angular/core';

import { SharedModule } from '@shared';
import { GioithieuRoutingModule } from './gioithieu-routing.module';
import { GioithieuComponent } from './gioithieu.component';

@NgModule({
  imports: [GioithieuRoutingModule, SharedModule],
  declarations: [GioithieuComponent],
})
export class GioithieuModule {}
