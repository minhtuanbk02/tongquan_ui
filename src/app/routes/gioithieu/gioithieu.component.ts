import { ChangeDetectorRef, Component } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';


@Component({
  templateUrl: './gioithieu.component.html',
})
export class GioithieuComponent {
  type = '';
  q = '';

  quick(key: string): void {
    this.q = key;
    this.search();
  }

  search(): void {
    this.msg.success(`搜索：${this.q}`);
  }

  constructor(public msg: NzMessageService) {}
}

