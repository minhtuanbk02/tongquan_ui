import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { QuytrinhnghiepvuRoutingModule } from './quytrinhnghiepvu-routing.module';
import { QuytrinhnghiepvuComponent } from './quytrinhnghiepvu.component';

@NgModule({
  imports: [QuytrinhnghiepvuRoutingModule, SharedModule, NzIconModule],
  declarations: [QuytrinhnghiepvuComponent],
})
export class QuytrinhnghiepvuModule {}
