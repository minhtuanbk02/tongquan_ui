import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { QuytrinhnghiepvuComponent } from './quytrinhnghiepvu.component';

const routes: Routes = [
  {
    path: '',
    component: QuytrinhnghiepvuComponent,
    data: {
      title: 'Quy trình đăng ký tích hợp',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuytrinhnghiepvuRoutingModule {}
