import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { EdocService } from '../../../../service/document.service';

@Component({
  templateUrl: './document-status.component.html',
})
export class DocumentStatusComponent implements OnInit {
  submitting = false;
  id: any;
  private data: any[] = [];
  indeterminate = false;
  checked = false;
  setOfCheckedId = new Set<number>();

  formDetail = this.fb.group({
    id: [null, []],
    fromOrganId: [null, []],
    organName: [null, []],
    toOrganId: [null, []],
    appId: [null, []],
    subject: [null, []],
    content: [null, []],
    dueDate: [null, []],
    timestamp: [null, []],
    documentId: [null, []],
    docId: [null, []],
    toPlaces: [null, []],
    documentType: [null, []],
    documentTypeName: [null, []],
    codeCodeNumber: [null, []],
    codeCodeNotation: [null, []],
    promulgationInfoDate: [null, []],
    promulgationInfoPlace: [null, []],
    steeringType: [null, []],
  });

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private edocService: EdocService,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });
    // detail
    this.edocService
      .initEdit(this.id)
      .then((response) => {
        this.formDetail.patchValue(response.data);
      })
      .catch((response: any) => {
        console.log(response);
      });
  }
  back(): void {
    this.router.navigateByUrl('/report/document-arrive');
  }
}
