import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';

import { ReportRoutingModule } from './report-routing.module';
import { DocumentArriveComponent } from './vanbanden/detail/document-arrive.component';
import { VanbandenComponent } from './vanbanden/vanbanden.component';
import { DocumentSendComponent } from './vanbandi/detail/document-send.component';
import { VanbandiComponent } from './vanbandi/vanbandi.component';
import { DocumentStatusComponent } from './vanbantrangthai/detail/document-status.component';
import { VanbanTrangThaiComponent } from './vanbantrangthai/vanbantrangthai.component';

const COMPONENTS = [
  VanbandenComponent,
  VanbandiComponent,
  DocumentSendComponent,
  DocumentArriveComponent,
  VanbanTrangThaiComponent,
  DocumentStatusComponent,
];

@NgModule({
  imports: [CommonModule, SharedModule, ReportRoutingModule],
  declarations: [...COMPONENTS],
})
export class ReportModule {}
