import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ACLGuard } from '@delon/acl';
import { DocumentArriveComponent } from './vanbanden/detail/document-arrive.component';
import { VanbandenComponent } from './vanbanden/vanbanden.component';
import { DocumentSendComponent } from './vanbandi/detail/document-send.component';
import { VanbandiComponent } from './vanbandi/vanbandi.component';
import { DocumentStatusComponent } from './vanbantrangthai/detail/document-status.component';
import { VanbanTrangThaiComponent } from './vanbantrangthai/vanbantrangthai.component';

const routes: Routes = [
  {
    path: 'document-send',
    component: VanbandiComponent,
    canActivate: [ACLGuard],
    data: { title: 'Văn bản đi' },
  },
  {
    path: 'document-send/detail/:id',
    component: DocumentSendComponent,
    canActivate: [ACLGuard],
    data: { title: 'Văn bản đi | chi tiết' },
  },
  {
    path: 'document-arrive',
    component: VanbandenComponent,
    canActivate: [ACLGuard],
    data: { title: 'Văn bản đến' },
  },
  {
    path: 'document-arrive/detail/:id',
    component: DocumentArriveComponent,
    canActivate: [ACLGuard],
    data: { title: 'Văn bản đến | chi tiết' },
  },
  {
    path: 'document-status',
    component: VanbanTrangThaiComponent,
    canActivate: [ACLGuard],
    data: { title: 'Văn bản trạng thái' },
  },
  {
    path: 'document-status/detail/:id',
    component: DocumentStatusComponent,
    canActivate: [ACLGuard],
    data: { title: 'Văn bản trạng thái | chi tiết' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportRoutingModule {}
