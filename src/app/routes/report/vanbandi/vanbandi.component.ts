import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { _HttpClient } from '@delon/theme';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Constants } from '../../../model/Constants';
import { ConstantsAuthor } from '../../../model/ConstantsAuthor';
import { EdocService } from '../../../service/document.service';

@Component({
  templateUrl: './vanbandi.component.html',
})
export class VanbandiComponent {
  role_add = ConstantsAuthor.Right.add;
  role_edit = ConstantsAuthor.Right.edit;
  role_delete = ConstantsAuthor.Right.delete;
  PAGE_SIZE = Constants.PAGE_SIZE;
  nzTotal = 0;
  confirmModal?: NzModalRef;
  submitting = false;
  search = { name: '', code: '', fromDate: null, toDate: null };
  loading = false;
  private data: any[] = [];
  indeterminate = false;
  checked = false;
  setOfCheckedId = new Set<number>();

  constructor(
    public msg: NzMessageService,
    private http: _HttpClient,
    private cdr: ChangeDetectorRef,
    private edocService: EdocService,
    private router: Router,
    private modal: NzModalService,
    private translateService: TranslateService,
  ) {}

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit(): void {
    this.getData(0, this.PAGE_SIZE);
  }

  onAllChecked(checked: boolean): void {
    this.data.filter(({ disabled }) => !disabled).forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.data.every((item) => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.data.some((item) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  changePageSize(size: any): void {
    this.PAGE_SIZE = size;
    this.getData(0, this.PAGE_SIZE);
  }

  getData(page: number, size: number): void {
    page = page - 1;
    this.loading = true;
    this.edocService
      .getPage(this.search, page, size, 0)
      .then((response) => {
        this.loading = false;
        this.data = response.data.content;
        this.nzTotal = response.data.totalElements;
        this.cdr.detectChanges();

        this.setOfCheckedId.clear();
        this.refreshCheckedStatus();
      })
      .catch((response: any) => {
        this.loading = false;
        console.log(response);
      });
  }

  reset(): void {
    setTimeout(() => this.getData(0, this.PAGE_SIZE));
  }

  edit(id: any): void {
    this.router.navigateByUrl('/report/document-send/detail/' + id);
  }
}
