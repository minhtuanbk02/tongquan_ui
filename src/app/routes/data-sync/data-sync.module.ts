import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SyncCalendarComponent } from './calendar/sync-calendar.component';
import { DataSyncRoutingModule } from './data-sync-routing.module';
import { SyncHistoryComponent } from './history/sync-history.component';

const COMPONENTS = [SyncCalendarComponent, SyncHistoryComponent];

@NgModule({
  imports: [CommonModule, DataSyncRoutingModule, BsDropdownModule.forRoot(), FormsModule, ReactiveFormsModule, SharedModule, ModalModule],
  declarations: [...COMPONENTS],
})
export class DataSyncModule {}
