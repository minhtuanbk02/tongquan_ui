import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { _HttpClient } from '@delon/theme';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Constants } from '../../../model/Constants';
import { SyncHistoryService } from '../../../service/sync-history.service';

@Component({
  selector: 'app-sync-history',
  templateUrl: './sync-history.component.html',
})
export class SyncHistoryComponent implements OnInit {
  listStatus = Constants.STATUS_SYNC_LIST;
  listjobName = Constants.JOB_NAME_LIST;
  PAGE_SIZE = Constants.PAGE_SIZE;
  nzTotal = 0;
  confirmModal?: NzModalRef;

  search = { jobName: '', status: '', fromDate: null, toDate: null };
  loading = false;
  data: any[] = [];
  indeterminate = false;
  checked = false;
  setOfCheckedId = new Set<number>();

  constructor(
    public msg: NzMessageService,
    private http: _HttpClient,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private syncHistoryService: SyncHistoryService,
    private modal: NzModalService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.getData(0, this.PAGE_SIZE);
  }

  onAllChecked(checked: boolean): void {
    this.data.filter(({ disabled }) => !disabled).forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.data.every((item) => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.data.some((item) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  changePageSize(size: any): void {
    this.PAGE_SIZE = size;
    this.getData(0, this.PAGE_SIZE);
  }

  getData(page: number, size: number): void {
    this.loading = true;
    this.syncHistoryService
      .getPage(this.search, page, size)
      .then((response) => {
        console.log(response);
        this.loading = false;
        this.data = response.data.content;
        this.nzTotal = response.data.totalElements;
        this.cdr.detectChanges();

        this.setOfCheckedId.clear();
        this.refreshCheckedStatus();
      })
      .catch((response: any) => {
        this.loading = false;
        console.log(response);
      });
  }

  reset(): void {
    setTimeout(() => this.getData(0, this.PAGE_SIZE));
  }
}
