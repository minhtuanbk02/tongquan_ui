import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { _HttpClient } from '@delon/theme';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BsDropdownDirective } from 'ngx-bootstrap/dropdown';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Constants } from '../../../model/Constants';
import { ConstantsAuthor } from '../../../model/ConstantsAuthor';
import { AdmRightService } from '../../../service/adm-right.service';
import { SyncCalendarService } from '../../../service/sync-calendar.service';

@Component({
  selector: 'app-sync-calendar',
  templateUrl: './sync-calendar.component.html',
  providers: [BsDropdownDirective],
})
export class SyncCalendarComponent {
  @ViewChild('dangerModal') public dangerModal!: ModalDirective;
  @ViewChild('largeModal') public largeModal!: ModalDirective;
  @ViewChild('stopModal') public stopModal!: ModalDirective;
  @ViewChild('runModal') public runModal!: ModalDirective;
  data: any[] = [];
  PAGE_SIZE = Constants.PAGE_SIZE;
  nzTotal = 0;
  confirmModal?: NzModalRef;
  id_delete = '';
  id_stop = '';
  id_run = '';

  search = { jobName: '' };

  loading_delete = false;
  loading = false;

  titleModal: any;
  isVisible = false;
  delVisible = false;
  itemUpdate: any = null;

  form = this.fb.group({
    id: [null],
    typeSyn: ['NGAY', [Validators.required]],
    dayOfMonth: [null, [Validators.required]],
    dayOfWeek: [null, [Validators.required]],
    time: [null, [Validators.required]],
    jobName: [null, [Validators.required, Validators.pattern('([0-9a-zA-Z]|\\.)+')]],
    unit: [null],
  });

  syncTypes = [
    { value: 'THANG', label: 'Hàng tháng' },
    { value: 'TUAN', label: 'Hàng tuần' },
    { value: 'NGAY', label: 'Hàng ngày' },
    { value: 'THOIGIAN', label: 'Theo khoảng thời gian' },
  ];

  daysInWeek = [
    { value: 2, label: 'Thứ 2' },
    { value: 3, label: 'Thứ 3' },
    { value: 4, label: 'Thứ 4' },
    { value: 5, label: 'Thứ 5' },
    { value: 6, label: 'Thứ 6' },
    { value: 7, label: 'Thứ 7' },
    { value: 1, label: 'Chủ nhật' },
  ];

  timeUnits = [
    { value: 'phut', label: 'Phút' },
    { value: 'gio', label: 'Giờ' },
    { value: 'ngay', label: 'Ngày' },
  ];
  listJob = [
    { jobName: 'Đồng bộ văn bản điện tử', id: 'JobSynEdoc' },
    { jobName: 'Đồng bộ văn bản trạng thái', id: 'JobSynStatus' },
    { jobName: 'Đồng bộ đơn vị liên thông', id: 'JobSynAgency' },
  ];
  onDetail = false;
  submitting = false;

  constructor(
    private syncService: SyncCalendarService,
    public msg: NzMessageService,
    private http: _HttpClient,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private modal: NzModalService,
    private translateService: TranslateService,
    private fb: FormBuilder,
  ) {
    this.getPage(1, this.PAGE_SIZE);
    this.refreshChangeType();
  }

  getPage(page: number, size: number): void {
    page = page - 1;
    this.loading = true;
    this.syncService.getPage(this.search, page, size).then((response) => {
      this.loading = false;
      this.data = response.data.content;
      this.nzTotal = response.data.totalElements;
      this.cdr.detectChanges();
    });
  }

  changePageSize(size: any): void {
    this.PAGE_SIZE = size;
    this.getPage(1, this.PAGE_SIZE);
  }

  openCreateUpdate(isUpdate: boolean, item?: any): void {
    if (isUpdate) {
      this.syncService
        .initEdit(item.id)
        .then((res) => {
          this.itemUpdate = res.data;
          console.log(this.itemUpdate.dayOfWeek);
          this.form.patchValue(this.itemUpdate);
          this.titleModal = 'Cập nhật lịch đồng bộ';
          this.isVisible = true;
          this.refreshChangeType();
        })
        .catch((res) => {
          console.log(res);
        })
        .finally(() => {
          console.log(this.form.value);
        });
    } else {
      if (item) {
        this.syncService.initEdit(item.id).then((res) => {
          this.itemUpdate = res.data;
          this.form.patchValue(this.itemUpdate);
          this.refreshChangeType();
        });
        this.onDetail = true;
        this.titleModal = 'Chi tiết lịch đồng bộ';
        this.isVisible = true;
      } else {
        this.titleModal = 'Thêm mới lịch đồng bộ';
        this.isVisible = true;
        this.refreshChangeType();
      }
    }
  }

  handleCancel(): void {
    this.isVisible = false;
    this.delVisible = false;
    this.itemUpdate = null;
    this.onDetail = false;
    this.form.reset();
    this.refreshChangeType();
  }

  delete(): void {
    this.delVisible = true;
  }

  submit(): void {
    this.submitting = true;
    if (!this.form.valid) {
      this.msg.warning('Vui lòng kiểm tra lại các trường.');
      return;
    }

    this.refreshChangeType();
    console.log(this.form.value);
    if (!this.itemUpdate) {
      this.syncService
        .add(this.form.value)
        .then((response) => {
          if (response.code === 409) {
            this.msg.error('Đã tồn tại lịch đồng bộ với tên này.');
          } else {
            this.msg.success('Thêm mới thành công.');
            this.handleCancel();
            this.getPage(1, this.PAGE_SIZE);
          }
          this.submitting = false;
        })
        .catch((response) => {
          console.log(response);
          this.msg.error('Có lỗi xảy ra.');
        });
    } else {
      this.syncService
        .edit(this.form.value)
        .then((response) => {
          if (response.code === 500) {
            this.msg.warning('Vui lòng kiểm tra lại dữ liệu.');
          } else {
            this.msg.success('Chỉnh sửa thành công.');
            this.handleCancel();
            this.getPage(1, this.PAGE_SIZE);
          }
          this.submitting = false;
        })
        .catch((response) => {
          console.log(response);
          this.msg.error('Có lỗi xảy ra.');
        });
    }
  }

  performDelete(id: any): void {
    const list: any[] = [];
    list.push(id);
    this.syncService
      .delete(list)
      .then((response) => {
        this.msg.success(this.translateService.instant('message.success'));
        this.getPage(0, this.PAGE_SIZE);
        this.dangerModal.hide();
      })
      .catch((res) => {
        console.log(res);
        this.msg.error('Có lỗi xảy ra.');
      });
  }

  performStop(id: any): void {
    const list: any[] = [];
    list.push(id);
    this.syncService
      .stop(list)
      .then((response) => {
        this.msg.success(this.translateService.instant('message.success'));
        this.getPage(0, this.PAGE_SIZE);
        this.stopModal.hide();
      })
      .catch((res) => {
        console.log(res);
        this.msg.error('Có lỗi xảy ra.');
      });
  }

  performRun(id: any): void {
    const list: any[] = [];
    list.push(id);
    this.syncService
      .resume(list)
      .then((response) => {
        this.msg.success(this.translateService.instant('message.success'));
        this.getPage(0, this.PAGE_SIZE);
        this.runModal.hide();
      })
      .catch((res) => {
        console.log(res);
        this.msg.error('Có lỗi xảy ra.');
      });
  }

  fetchTypeSyncName(name: any): string {
    switch (name) {
      case 'THANG':
        return 'Hàng tháng';
      case 'NGAY':
        return 'Hàng ngày';
      case 'TUAN':
        return 'Hàng tuần';
      case 'THOIGIAN':
        return 'Theo khoảng thời gian';
    }
    return name;
  }

  fetchName(id: any): string {
    return this.listJob.filter((x) => x.id === id)[0].jobName;
  }

  refreshChangeType(): void {
    switch (this.form.value.typeSyn) {
      case 'THANG':
        this.form.patchValue({
          dayOfWeek: null,
          unit: null,
        });

        this.form.controls.dayOfWeek.setValidators([]);
        this.form.controls.dayOfMonth.setValidators([Validators.required]);
        this.form.controls.unit.setValidators([]);
        this.form.controls.time.setValidators([Validators.required]);

        break;
      case 'TUAN':
        this.form.patchValue({
          dayOfMonth: null,
          unit: null,
        });

        this.form.controls.dayOfWeek.setValidators([Validators.required]);
        this.form.controls.dayOfMonth.setValidators([]);
        this.form.controls.unit.setValidators([]);
        this.form.controls.time.setValidators([Validators.required]);

        break;
      case 'NGAY':
        this.form.patchValue({
          dayOfWeek: null,
          dayOfMonth: null,
          unit: null,
        });

        this.form.controls.dayOfWeek.setValidators([]);
        this.form.controls.dayOfMonth.setValidators([]);
        this.form.controls.unit.setValidators([]);
        this.form.controls.time.setValidators([Validators.required]);

        break;
      case 'THOIGIAN':
        this.form.patchValue({
          dayOfWeek: null,
          dayOfMonth: null,
        });

        this.form.controls.dayOfWeek.setValidators([]);
        this.form.controls.dayOfMonth.setValidators([]);
        this.form.controls.unit.setValidators([Validators.required]);
        this.form.controls.time.setValidators([Validators.required]);

        break;
    }
    this.form.controls.dayOfWeek.updateValueAndValidity();
    this.form.controls.dayOfMonth.updateValueAndValidity();
    this.form.controls.unit.updateValueAndValidity();
    this.form.controls.time.updateValueAndValidity();
  }
}
