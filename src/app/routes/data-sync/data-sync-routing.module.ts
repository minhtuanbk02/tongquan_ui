import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SyncCalendarComponent } from './calendar/sync-calendar.component';
import { SyncHistoryComponent } from './history/sync-history.component';

const routes: Routes = [
  { path: 'calendar', component: SyncCalendarComponent, data: { title: 'Đồng bộ liên thông' } },
  { path: 'history', component: SyncHistoryComponent, data: { title: 'Lịch sử đồng bộ' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataSyncRoutingModule {}
