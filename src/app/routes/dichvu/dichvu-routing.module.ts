import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DichvuComponent } from './dichvu.component';

const routes: Routes = [
  {
    path: '',
    component: DichvuComponent,
    data: {
      title: 'Dịch vụ chia sẻ',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DichvuRoutingModule {}
