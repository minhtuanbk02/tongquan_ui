import { ChangeDetectorRef, Component } from '@angular/core';
import {FormGroup} from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';


@Component({
  templateUrl: './dichvu.component.html',
})
export class DichvuComponent {
  type = '';
  q = '';
  form!: FormGroup;

  quick(key: string): void {
    this.q = key;
    this.search();
  }

  search(): void {
    this.msg.success(`搜索：${this.q}`);
  }

  constructor(public msg: NzMessageService) {}
}

