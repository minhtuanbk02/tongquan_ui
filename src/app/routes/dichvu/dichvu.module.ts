import { NgModule } from '@angular/core';
import {AppHeaderModule} from '@coreui/angular';

import { SharedModule } from '@shared';
import { DichvuRoutingModule } from './dichvu-routing.module';
import { DichvuComponent } from './dichvu.component';

@NgModule({
  imports: [DichvuRoutingModule, SharedModule, AppHeaderModule],
  declarations: [DichvuComponent],
})
export class DichvuModule {}
