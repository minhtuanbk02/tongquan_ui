import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UngdungComponent } from './ungdung.component';

const routes: Routes = [
  {
    path: '',
    component: UngdungComponent,
    data: {
      title: 'Ứng dụng kết nối',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UngdungRoutingModule {}
