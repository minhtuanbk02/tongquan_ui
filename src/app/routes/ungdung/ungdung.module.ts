import { NgModule } from '@angular/core';

import { SharedModule } from '@shared';
import { UngdungRoutingModule } from './ungdung-routing.module';
import { UngdungComponent } from './ungdung.component';

@NgModule({
  imports: [UngdungRoutingModule, SharedModule],
  declarations: [UngdungComponent],
})
export class UngdungModule {}
