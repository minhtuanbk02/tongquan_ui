import {HttpHeaders, HttpParams} from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { _HttpClient } from '@delon/theme';
import { AppConfig } from '../app.config';
import {Constants, HeaderField, HeaderValue} from '../model/Constants';
import { CommonService } from './common.service';

@Injectable({ providedIn: 'root' })
export class SsoService extends CommonService {
  SSO_IS_URL = AppConfig.settings.SSO_IS_URL;
  BACKEND_URL = AppConfig.settings.BACKEND_URL;
  constructor(private http: _HttpClient, router: Router, @Inject(DA_SERVICE_TOKEN) tokenService: ITokenService) {
    super(router, tokenService);
  }

  initSso(): void {
    window.location.href =
      AppConfig.settings.SSO_IS_URL +
      '/oauth2/authorize?client_id=' +
      AppConfig.settings.SSO_CLIENT_ID +
      '&scope=openid&redirect_uri=' +
      encodeURIComponent(AppConfig.settings.SSO_CALLBACK_URL) +
      '&response_type=code';
  }

  getPage(search: any, page: number, size: number): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    let param = new HttpParams();
    param = param.set('_allow_anonymous', 'true');
    param = param.set('search', JSON.stringify(search));
    param = param.set('size', size.toString());
    param = param.set('page', page.toString());
    const promise = this.http
      .get(this.SSO_IS_URL + '/getPage', { headers: secureHeaders, params: param })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  userSearch(username: any): Promise<any> {
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.BACKEND_URL + '/public/v1/scim2/Users/.search' + '?_allow_anonymous=true', username, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  sendOtp(obj: any): Promise<any> {
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.BACKEND_URL + '/public/v1/scim2/Users/.sendOtp' + '?_allow_anonymous=true', obj, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  checkOtp(obj: any): Promise<any> {
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.BACKEND_URL + '/public/v1/scim2/Users/.checkOtp' + '?_allow_anonymous=true', obj, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  createUser(obj: any): Promise<any> {
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.BACKEND_URL + '/public/v1/scim2/Users/.createUser' + '?_allow_anonymous=true', obj, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }
}
