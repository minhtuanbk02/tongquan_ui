import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { _HttpClient } from '@delon/theme';
import { AppConfig } from '../app.config';
import { HeaderField, HeaderValue } from '../model/Constants';
import { CommonService } from './common.service';

@Injectable({ providedIn: 'root' })
export class AdmAuthorityService extends CommonService {
  authorityApi = AppConfig.settings.BACKEND_URL + '/api/system/v1/authorities';

  constructor(private http: HttpClient, router: Router, @Inject(DA_SERVICE_TOKEN) tokenService: ITokenService) {
    super(router, tokenService);
  }

  getPage(search: any, page: number, size: number): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    let param = new HttpParams();
    param = param.set('_allow_anonymous', 'true');
    param = param.set('search', JSON.stringify(search));
    param = param.set('size', size.toString());
    param = param.set('page', page.toString());
    const promise = this.http
      .get(this.authorityApi + '/getPage', { headers: secureHeaders, params: param })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  loadListParent(): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .get(this.authorityApi + '/loadListParent?_allow_anonymous=true', { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  loadChildrenByParentId(parentId: any): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    let param = new HttpParams();
    param = param.set('_allow_anonymous', 'true');
    param = param.set('parentId', parentId);
    const promise = this.http
      .get(this.authorityApi + '/loadChildrenByParentId', { headers: secureHeaders, params: param })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  add(form: any): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.authorityApi + '?_allow_anonymous=true', form, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  initEdit(groupId: any): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .get(this.authorityApi + '/initEdit/' + groupId + '?_allow_anonymous=true', { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  edit(form: any): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .put(this.authorityApi + '?_allow_anonymous=true', form, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  delete(ids: any): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.authorityApi + '/delete' + '?_allow_anonymous=true', ids, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  lock(ids: any): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.authorityApi + '/lock' + '?_allow_anonymous=true', ids, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  unlock(ids: any): Promise<any> {
    const accessToken = this.getAccessToken();
    let secureHeaders = new HttpHeaders();
    secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.authorityApi + '/unlock' + '?_allow_anonymous=true', ids, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }
}
