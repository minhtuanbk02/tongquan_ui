import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { _HttpClient } from '@delon/theme';
import { AppConfig } from '../app.config';
import { Constants } from '../model/Constants';
import { CommonService } from './common.service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService extends CommonService {
  userApi = AppConfig.settings.BACKEND_URL + '/public/v1/scim2';

  constructor(private http: HttpClient, router: Router, @Inject(DA_SERVICE_TOKEN) tokenService: ITokenService) {
    super(router, tokenService);
  }

  login(body: any): Promise<any> {
    const promise = this.http
      .post(this.userApi + '/login?_allow_anonymous=true', body)
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  logout(): Promise<any> {
    const secureHeaders = new Headers();
    // secureHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
    const token = this.getAccessToken();
    const promise = this.http
      .post(this.userApi + '/logout?_allow_anonymous=true', token)
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  getUserInfo(authorizationCode: string): Promise<any> {
    const promise = this.http
      .post(this.userApi + '/user-info?_allow_anonymous=true', authorizationCode)
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  getAccessTokenByRefreshToken(refreshToken: any): Promise<any> {
    const promise = this.http
      .post(this.userApi + '/access-token?_allow_anonymous=true', refreshToken)
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }
}
