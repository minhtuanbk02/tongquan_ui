import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { _HttpClient } from '@delon/theme';
import { AppConfig } from '../app.config';
import { HeaderField, HeaderValue } from '../model/Constants';
import { CommonService } from './common.service';

@Injectable({ providedIn: 'root' })
export class AgencyService extends CommonService {
  agecyApi = AppConfig.settings.BACKEND_URL + '/lgsp/ltvb/agency';

  constructor(private http: HttpClient, router: Router, @Inject(DA_SERVICE_TOKEN) tokenService: ITokenService) {
    super(router, tokenService);
  }

  getPage(search: any, page: number, size: number): Promise<any> {
    const secureHeaders = new HttpHeaders();
    // const accessToken = this.getAccessToken();
    // secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    //  secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    let param = new HttpParams();
    param = param.set('_allow_anonymous', 'true');
    param = param.set('search', JSON.stringify(search));
    param = param.set('size', size.toString());
    param = param.set('page', page.toString());
    const promise = this.http
      .get(this.agecyApi + '/getPage', { headers: secureHeaders, params: param })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  getListCenter(): Promise<any> {
    //  const accessToken = this.getAccessToken();
    const secureHeaders = new HttpHeaders();
    // secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    // secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .get(this.agecyApi + '/listCenter?_allow_anonymous=true', { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }
  listAgencyVP(): Promise<any> {
    const secureHeaders = new HttpHeaders();
    const promise = this.http
      .get(this.agecyApi + '/listAgencyVP?_allow_anonymous=true', { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  add(form: any): Promise<any> {
    // const accessToken = this.getAccessToken();
    const secureHeaders = new HttpHeaders();
    // secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    // secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.agecyApi + '/save' + '?_allow_anonymous=true', form, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  initEdit(id: any): Promise<any> {
    const secureHeaders = new HttpHeaders();
    // const accessToken = this.getAccessToken();
    // let secureHeaders = new HttpHeaders();
    // secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    // secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    //
    const promise = this.http
      .get(this.agecyApi + '/initEdit/' + id + '?_allow_anonymous=true', { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  delete(body: any): Promise<any> {
    // const accessToken = this.getAccessToken();
    const secureHeaders = new HttpHeaders();
    //  secureHeaders = secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    // secureHeaders = secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    const promise = this.http
      .post(this.agecyApi + '/delete' + '?_allow_anonymous=true', body, { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }
}
