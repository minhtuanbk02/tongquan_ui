import { ElementRef, Inject, Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';

@Injectable({ providedIn: 'root' })
export class CommonService {
  constructor(protected router: Router, @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService) {}

  public handleError(error: any): Promise<any> {
    error = error;
    // if stauts error is 403 (Access Denied)
    if (error.status === 403) {
      // this.router.navigate(['/auth/login']);
    }
    return Promise.reject(error);
  }

  public getAccessToken(): any {
    const tokenService = this.getITokenService();
    return tokenService.token;
  }

  public getRefreshToken(): any {
    const tokenService = this.getITokenService();
    return tokenService.refreshToken;
  }

  public getITokenService(): any {
    const tokenService = this.tokenService.get();
    return tokenService;
  }

  public isNull(obj: any): boolean {
    if (typeof obj == 'undefined' || obj == null || obj == '') {
      return true;
    }
    return false;
  }

  public isNotNull(obj: any): boolean {
    if (typeof obj != 'undefined' && obj != null && obj != '') {
      return true;
    }
    return false;
  }

  public validateForm(form: FormGroup, el: ElementRef): boolean {
    let check = true;
    for (const field of Object.keys(form.controls)) {
      const control = form.get(field);
      const validate = control?.markAsTouched({ onlySelf: true });
      if (form.controls[field].invalid && check) {
        check = false;
        const invalidControl = el.nativeElement.querySelector('[formcontrolname="' + field + '"]');
        invalidControl.focus();
      }
    }
    return check;
  }
}
