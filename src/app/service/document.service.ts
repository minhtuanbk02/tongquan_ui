import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { _HttpClient } from '@delon/theme';
import { AppConfig } from '../app.config';
import { HeaderField, HeaderValue } from '../model/Constants';
import { CommonService } from './common.service';

@Injectable({ providedIn: 'root' })
export class EdocService extends CommonService {
  agecyApi = AppConfig.settings.BACKEND_URL + '/lgsp/ltvb/document';

  constructor(private http: HttpClient, router: Router, @Inject(DA_SERVICE_TOKEN) tokenService: ITokenService) {
    super(router, tokenService);
  }

  getPage(search: any, page: number, size: number, sender: any): Promise<any> {
    const secureHeaders = new HttpHeaders();
    let param = new HttpParams();
    param = param.set('_allow_anonymous', 'true');
    param = param.set('search', JSON.stringify(search));
    param = param.set('size', size.toString());
    param = param.set('page', page.toString());
    param = param.set('sender', sender);
    const promise = this.http
      .get(this.agecyApi + '/getPage', { headers: secureHeaders, params: param })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  getPageStatus(search: any, page: number, size: number): Promise<any> {
    const secureHeaders = new HttpHeaders();
    let param = new HttpParams();
    param = param.set('_allow_anonymous', 'true');
    param = param.set('search', JSON.stringify(search));
    param = param.set('size', size.toString());
    param = param.set('page', page.toString());
    const promise = this.http
      .get(this.agecyApi + '/getPageStatus', { headers: secureHeaders, params: param })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }

  initEdit(id: any): Promise<any> {
    const secureHeaders = new HttpHeaders();
    const promise = this.http
      .get(this.agecyApi + '/initEdit/' + id + '?_allow_anonymous=true', { headers: secureHeaders })
      .toPromise()
      .then((response) => response as any)
      .catch((error) => {
        return this.handleError(error);
      });
    return promise;
  }
}
