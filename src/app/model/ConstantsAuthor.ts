export class ConstantsAuthor {
  static Dept = {
    // người dùng
    view: 'ROLE_SYSTEM_DEPT_VIEW',
    add: 'ROLE_SYSTEM_DEPT_ADD',
    edit: 'ROLE_SYSTEM_DEPT_EDIT',
    delete: 'ROLE_SYSTEM_DEPT_DELETE',
  };

  static User = {
    // người dùng
    view: 'ROLE_SYSTEM_USER_VIEW',
    add: 'ROLE_SYSTEM_USER_ADD',
    edit: 'ROLE_SYSTEM_USER_EDIT',
    delete: 'ROLE_SYSTEM_USER_DELETE',
  };

  static Group = {
    // nhóm quyền
    view: 'ROLE_SYSTEM_GROUP_VIEW',
    add: 'ROLE_SYSTEM_GROUP_ADD',
    edit: 'ROLE_SYSTEM_GROUP_EDIT',
    delete: 'ROLE_SYSTEM_GROUP_DELETE',
  };

  static Authority = {
    // Quyền
    view: 'ROLE_SYSTEM_AUTHORITY_VIEW',
    edit: 'ROLE_SYSTEM_AUTHORITY_EDIT',
    add: 'ROLE_SYSTEM_AUTHORITY_ADD',
    delete: 'ROLE_SYSTEM_AUTHORITY_DELETE',
  };

  static Right = {
    // menu
    view: 'ROLE_SYSTEM_RIGHT_VIEW',
    edit: 'ROLE_SYSTEM_RIGHT_EDIT',
    add: 'ROLE_SYSTEM_RIGHT_ADD',
    delete: 'ROLE_SYSTEM_RIGHT_DELETE',
  };

  static Param_System = {
    // Param_System
    view: 'ROLE_SYSTEM_PARAM_VIEW',
    edit: 'ROLE_SYSTEM_PARAM_EDIT',
    add: 'ROLE_SYSTEM_PARAM_ADD',
    delete: 'ROLE_SYSTEM_PARAM_DELETE',
  };

  static Report = {
    alarms: 'ROLE_REPORT_ALARMS',
    ticket: 'ROLE_REPORT_TICKET',
  };
}
