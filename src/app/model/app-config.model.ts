export interface IAppConfig {
  SSO_IS_URL: string;
  SSO_CLIENT_ID: string;
  SSO_CALLBACK_URL: string;
  BACKEND_URL: string;
  USER_REPORT: string;
}
