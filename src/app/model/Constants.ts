export class Constants {
  static PAGE_SIZE = 10;
  static CODE_CENTER = '00.62.W00';
  static AUTHORIZATION_CODE = 'authorizationCode';
  static IS_AUTHENTIC = 'isAuthentic';
  static CURRENT_USER = 'currentUser';
  static ACCESS_TOKEN = 'accessToken';
  static REFRESH_TOKEN = 'refreshToken';
  static EXPIRE_TIME = 'expireTime';
  static SESSION_STATE = 'sessionState';
  static KEY_LANGUAGE = 'keyLanguage';
  static RETURN_URL = 'returnUrl';

  static STATUS_LIST = [
    { id: 0, name: 'Đang sử dụng' },
    { id: 1, name: 'Đã khóa' },
  ];
  static STATUSDOCUMENT_LIST = [
    { id: 0, name: 'Chưa nhận' },
    { id: 1, name: 'Đã nhận' },
  ];
  static STATUS_LIST_EDOC = [
    { id: 0, name: 'Văn bản trạng thái đi' },
    { id: 1, name: 'Văn bản trạng thái đến' },
  ];
  static STATUS_SYNC_LIST = [
    { id: null, name: 'Tất cả' },
    { id: 'Thành công', name: 'Thành công' },
    { id: 'Thất bại', name: 'Thất bại' },
  ];
  static JOB_NAME_LIST = [
    { id: null, name: 'Tât cả' },
    { id: 'EDOC', name: 'Đồng bộ văn bản điện tử' },
    { id: 'STATUS', name: 'Đồng bộ văn bản trạng thái' },
    { id: 'AGENCY', name: 'Đồng bộ đơn vị liên thông' },
  ];
  static STATUS_LIST_FULL = [
    { id: 0, name: 'Đang sử dụng' },
    { id: 1, name: 'Đã khóa' },
    { id: 2, name: 'Ngừng sử dụng' },
  ];
  static active = 0;
  static lock = 1;
  static de_active = 2;

  static NAME_PATTERN =
    '(( *)[a-zA-Z*%^!@#$:;&=()\'\\-`.+,/"0-9_-ẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹý]( *)+)*$';
  // [^a-z]*[a-z]*[^a-z]
  static CODE_PATTERN =
    '^( *)[^a-zA-Z*%^!@#$:;&=()\'\\-`.+,/"0-9_-ẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹý]*[a-zA-Z*%^!@#$:;&=()\'\\-`.+,/"0-9_-ẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹý]*[^a-zA-Z*%^!@#$:;&=()\'\\-`.+,/"0-9_-ẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹý]*$';
  static NUMBER_PATTERN = '^(0|[1-9][0-9.]*)$';
}

export class HeaderField {
  static AUTHORIZATION = 'Authorization';
  static CONTENT_TYPE = 'Content-Type';
}
export class HeaderValue {
  static ALL_VALUE = '*/*';
  static APPLICATION_ATOM_XML_VALUE = 'application/atom+xml';
  static APPLICATION_FORM_URLENCODED_VALUE = 'application/x-www-form-urlencoded';
  static APPLICATION_JSON_VALUE = 'application/json';
  static APPLICATION_JSON_UTF8_VALUE = 'application/json;charset=UTF-8';
  static APPLICATION_OCTET_STREAM_VALUE = 'application/octet-stream';
  static APPLICATION_PDF_VALUE = 'application/pdf';
  static APPLICATION_RSS_XML_VALUE = 'application/rss+xml';
  static APPLICATION_XHTML_XML_VALUE = 'application/xhtml+xml';
  static APPLICATION_XML_VALUE = 'application/xml';
  static IMAGE_GIF_VALUE = 'image/gif';
  static IMAGE_JPEG_VALUE = 'image/jpeg';
  static IMAGE_PNG_VALUE = 'image/png';
  static MULTIPART_FORM_DATA_VALUE = 'multipart/form-data';
  static TEXT_EVENT_STREAM_VALUE = 'text/event-stream';
  static TEXT_HTML_VALUE = 'text/html';
  static TEXT_MARKDOWN_VALUE = 'text/markdown';
  static TEXT_PLAIN_VALUE = 'text/plain';
  static TEXT_XML_VALUE = 'text/xml';
  static PARAM_QUALITY_FACTOR = 'q';
}
