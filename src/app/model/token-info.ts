export interface TokenInfo {
  accessToken: string;
  refreshToken: string;
  expiresIn: number;
  idToken: string;
}
