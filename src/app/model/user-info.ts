import { TokenInfo } from './token-info';

export interface UserInfo {
  userName: string;
  loweredUsername: string;
  mobileAlias: string;
  isAnonymous: number;
  userType: number;
  accessTokenInfo: TokenInfo;
  rights: any;
}
