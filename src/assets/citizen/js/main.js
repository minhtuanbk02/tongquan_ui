
var Obj = {};

(function($) {
  "use strict";

  /************************************************************
   * Predefined letiables
   *************************************************************/
  let $window = $(window),
    $body       = $('body'),
    $html       = $('html'),
    $document   = $(document),
    screen      = $window.outerWidth();

  /**
   * exists - HungLV
   * @return true
   */
  $.fn.exists = function() {
    return this.length > 0;
  };

  /**
   * isMobile - Check mobile screen - HungLV
   * @return void
   */
  $.fn.isMobile = function() {
    let $screen = $window.outerWidth();

    if ( $screen < 575 ) {
      return true;
    }
    return false;
  };

  /**
   * uaSetting - HungLV
   * @return void
   */
  Obj.uaSetting = function() {
    let _ua = (function(u) {
      return {
        Tablet: (u.indexOf('windows') !== -1 && u.indexOf('touch') !== -1 && u.indexOf('tablet pc') === -1) ||
          u.indexOf('ipad') !== -1 ||
          (u.indexOf('android') !== -1 && u.indexOf('mobile') === -1) ||
          (u.indexOf('firefox') !== -1 && u.indexOf('tablet') !== -1) ||
          u.indexOf('kindle') !== -1 ||
          u.indexOf('silk') !== -1 ||
          u.indexOf('playbook') !== -1,
        Mobile: (u.indexOf('windows') !== -1 && u.indexOf('phone') !== -1) ||
          u.indexOf('iphone') !== -1 ||
          u.indexOf('ipod') !== -1 ||
          (u.indexOf('android') !== -1 && u.indexOf('mobile') !== -1) ||
          (u.indexOf('firefox') !== -1 && u.indexOf('mobile') !== -1) ||
          u.indexOf('blackberry') !== -1,
      }
    })(window.navigator.userAgent.toLowerCase());
    if (_ua.Tablet || _ua.Mobile) {
      $body.addClass('sp');
    }
  };

  /**
   * common
   * @return {[type]}
   */
  Obj.common = function() {
    $("input[placeholder]").each(function () {
      $(this).attr("data-placeholder", this.placeholder);

      $(this).bind("focus", function () {
        this.placeholder = '';
      });
      $(this).bind("blur", function () {
        this.placeholder = $(this).attr("data-placeholder");
      });
    });
  };

  /**
   * initBtnGoTop
   * @return {void}
   */
  Obj.initBtnGoTop = function() {
    let elGoTop = $('.btn__gotop');
    if (elGoTop.exists() && !elGoTop.isMobile()) {
      checkOffsetEL($window);

      $window.scroll(function() {
        checkOffsetEL($(this));
      });
      clickBtn();
    } else {
      clickBtn();
    }

    function checkOffsetEL($obj) {
      if ($obj.scrollTop() > $body.height() / 3) {
        elGoTop.fadeIn(300);
      } else {
        elGoTop.fadeOut(300);
      }
    }

    function clickBtn() {
      elGoTop.click(function() {
        $('body, html').animate({ scrollTop: 0 }, 500);
      });
    }
  };

  /**
   * initAniScroll
   * @return {[type]}
   */
  Obj.initAniScroll = function() {
    let scrollOff = $('.scrollToggle'),
      windowsTop = $window.scrollTop(),
      wh = $window.height();
    if (scrollOff.exists()) {
      scrollOff.each(function() {
        let scrollOffTop = $(this).offset().top;
        $(this).addClass('ani--scrollOff');
        if (windowsTop + wh - 20 > scrollOffTop && $(this).hasClass('ani--scrollOff')) {
          $(this).removeClass('ani--scrollOff').addClass('ani--scrollOn');
        } else {
          $(this).removeClass('ani--scrollOn').addClass('ani--scrollOff');
        }
      });
    }
  };

  /**
   * handleMenuClick
   * @return {[type]}
   */
  Obj.handleMenuClick = function() {
    let el   = $('#menu-button');
    if (el.exists()) {
      el.bind('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if ($(this).hasClass('open')) {
          $(this).removeClass('open').next().stop(true, true).slideUp(150).removeClass('open');
          $body.removeClass('hidden');
        } else {
          $(this).addClass('open').next().stop(true, true).slideDown(150).addClass('open');
          $body.addClass('hidden');
        }
      });
    }
  };
  Obj.handleMenuFtClick = function() {
    let el   = $('.btn-dropdown');
    if (el.exists()) {
      el.bind('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if ($(this).hasClass('active')) {
          $(this).removeClass('active').next().stop(true, true).slideUp(150).removeClass('active');
        } else {
          $(this).addClass('active').next().stop(true, true).slideDown(150).addClass('active');
        }
      });
    }
    $("body").click
    (
      function(e)
      {
        if(e.target.className !== ".sub-dropdown")
        {
          $('.btn-dropdown').removeClass('active').next().stop(true, true).slideUp(150).removeClass('active');
        }
      }
    );
  };

  /**
   * handleDeviceResize
   * @return {[type]}
   */
  Obj.handleOpenChat = function() {
    let el   = $('#btn-chat-open');
    if (el.exists()) {
      el.bind('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if ($(this).hasClass('open')) {
          $(this).removeClass('open');
          $('.form-chat .box-content').stop(true, true).slideUp(150).removeClass('open');
        } else {
          $(this).addClass('open');
          $('.form-chat .box-content').stop(true, true).slideDown(150).addClass('open');
        }
      });

    }
  };

  /**
   * initSlider
   * @return {void}
   */
  Obj.initSlider = function() {
    // Slider Service
    let $res = [
      { breakpoint: 992, settings: { slidesToShow: 4, slidesToScroll: 1 } },
      { breakpoint: 575, settings: { slidesToShow: 2, slidesToScroll: 1 } }
    ];

    initSliderData($('.news-slider'), 5, 1, false, true, 20, true, $res);

    // Setting Slick slider
    function initSliderData($el, $num_show = 1, $num_scroll = 1, $dot = false, $arrow = true, $padding = 0, $autoplay = false, $res = []) {
      if ($el.exists()) {
        $el.slick({
          slidesToShow: $num_show,
          slidesToScroll: $num_scroll,
          arrows: $arrow,
          dots: $dot,
          centerPadding: $padding,
          draggable: false,
          autoplay: $autoplay,
          prevArrow: '<button type="button" class="slider-nav slider-nav--prev"></button>',
          nextArrow: '<button type="button" class="slider-nav slider-nav--next"></button>',
          responsive: $res
        });
      }
    }
  };

  Obj.handleTabsClick = function() {
    let el = $('#tabs a');
    if (el.exists()) {
      el.bind('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#tabs a').parent().removeClass('active');
        $(this).parent().addClass('active');
        let id = $(this).attr("href");
        $('.box-video-el').removeClass('active');
        $(id + ".box-video-el").addClass('active');
      });
    }
  };

  Obj.handleSelectClick = function() {
    let el = $('select');
    if (el.exists()) {
      $('select').each(function(){
        var $this = $(this), numberOfOptions = $(this).children('option').length;

        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');

        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
          'class': 'select-options'
        }).insertAfter($styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
          $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
          }).appendTo($list);
        }

        var $listItems = $list.children('li');

        $styledSelect.click(function(e) {
          e.stopPropagation();
          $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
          });
          $(this).toggleClass('active').next('ul.select-options').toggle();
        });

        $listItems.click(function(e) {
          e.stopPropagation();
          $styledSelect.text($(this).text()).removeClass('active');
          $this.val($(this).attr('rel'));
          $list.hide();
        });

        $(document).click(function() {
          $styledSelect.removeClass('active');
          $list.hide();
        });

      });
    }
  };

  /************************************************************
   * Obj Window load, ready, scroll, resize and functions
   *************************************************************/
  //Window load functions
  //
  $window.on('load', function() {
    Obj.uaSetting();

    Obj.initAniScroll();
  });
  //Document ready functions
  $document.ready(function() {
    Obj.common();
    Obj.handleMenuClick();
    Obj.handleOpenChat();
    Obj.initBtnGoTop();
    Obj.initSlider();
    Obj.handleMenuFtClick();
    Obj.handleTabsClick();
    Obj.handleSelectClick();
  });

  //Window scroll functions
  $window.on('scroll', function() {
    Obj.initAniScroll();
  });

  //Window resize functions
  $window.on('resize', function() {
    Obj.handleOpenChat();
  });

  window.Obj= Obj;

})(jQuery);

$(function() {
  $(".img-hover img")
    .mouseover(function() {
      var src = $(this).attr("src").match(/[^\.]+/) + "_hover.png";
      $(this).attr("src", src);
    })
    .mouseout(function() {
      var src = $(this).attr("src").replace("_hover.png", ".png");
      $(this).attr("src", src);
    });
});

$(document).ready(function () {
  $(".btn-open-sub").hover(
    function () {
      $(this).children().next().slideDown('fast');
    },
    function () {
      $(this).children().next().slideUp('fast');
    }
  );
});
